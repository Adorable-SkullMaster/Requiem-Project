import os
import discord
import traceback
from core.error_handling import raise_error


async def modulehandler(bot, type):
    passed = True
    exts = [I[:-3] for I in os.listdir("../src/modules") if I.endswith(".py")]

    async def load(m):
        try:
            bot.load_extension(f"src.modules.{m}")
            bot.logger.info(f"{m} Loaded Successfully!")
        except Exception as e:
            await raise_error(bot, e, f"an extension: {m}")
            return False

    async def unload(m):
        bot.unload_extension(f"src.modules.{m}")

    if type == "start":
        for i in exts:
            await load(i)
    elif type == "stop":
        for i in exts:
            await unload(i)
    elif type == "cycle":
        for i in exts:
            await unload(i)
            resp = await load(i)
            if resp is False:
                passed = False
        return passed
    else:
        if type in exts:
            await unload(type)
            resp = await load(type)
            if resp is False:
                passed = False
            return passed
        else:
            return "BadInput"
