import json
import os

def rsshandle(rs, input=None):
    def resources(loc):
        return [I for I in os.listdir(loc)]
    locs = ["data/stash/", "data/resources/"]
    try:
        for loc in locs:
            rss = resources(loc)
            if rs in rss:
                if rs.endswith(".json"):
                    if input is not None:
                        with open(loc + rs, 'w') as dmp:
                            json.dump(input, dmp)
                    else:
                        with open(loc + rs) as rd:
                            return json.load(rd)
                else:
                    result = open(loc + rs, "r")
                    return result.read()
    except:
        print(f"There was an issue attempting to read/write the file {rs}")
