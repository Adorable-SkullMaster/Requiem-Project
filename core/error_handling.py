import traceback
import random
import json
import discord
from discord.ext import commands
from core.resourcehandler import rsshandle


def credentials():
    with open("../credentials.json") as credentials:
        data = json.load(credentials)
    return data


async def log_error(bot, title, tb):
    bot.er = bot.er + 1
    starttime = str(bot.starttime).replace(".", "")
    f = open(f"data/logs/{starttime}-{bot.er}.txt", "w+")
    f.write(f"{title}\n\n{tb}")
    f.close()

async def raise_error(bot, error, title, ctx=None):
    print(error)
    if isinstance(
            error,
            (
                    commands.CommandOnCooldown,
                    commands.MissingPermissions,
                    commands.BotMissingPermissions,
                    commands.NotOwner,
            ),
    ):
        await ctx.send(embed=discord.Embed(description=f"{error}", color=0xe40000))
    elif isinstance(
            error,
            (
                    commands.CommandNotFound,
                    commands.MissingRequiredArgument,
                    commands.BadArgument,
                    commands.CheckFailure,
            ),
    ):
        pass
    elif type(error) is discord.Forbidden:
        pass
    elif error.__cause__.__class__.__name__ == "Forbidden":
        await ctx.send(
            embed=discord.Embed(
                description="I do not have the permissions to do that!", color=0xe40000
            )
        )
    else:
        await error_report(bot, error, title, ctx)


async def error_response(ctx):
    responses = rsshandle("general_responses.json")
    output = (
            random.choice(responses["error"])
            + "\n\nAn error report has been submitted! Sorry for the inconvenience!"
    )
    await ctx.send(embed=discord.Embed(description=output, color=0xe40000))


async def error_report(bot, error, title, ctx):
    if ctx is not None:
        await error_response(ctx)
    try:
        data = rsshandle('errors.json')
    except:
        data = []
    error = error.__cause__ or error
    tb = traceback.format_exception(
        type(error), error, error.__traceback__, chain=False
    )
    tb = "".join(tb)
    if title not in data:
        if title != "a command: 'error'":
            data.append(title)
            rsshandle('errors.json', data)
        user = bot.get_user(credentials()["ownerid"])
        embed = discord.Embed(
            title=f"An error occurred while processing {title}",
            color=0xe40000
        )
        if len(tb) > 1000:
            await user.send(
                embed=embed.add_field(
                    name="Warning:",
                    value="The traceback was too large! Check the console for error details!",
                )
            )
        else:
            await user.send(
                embed=embed.add_field(
                    name=type(error).__name__, value=f"```\n{tb}\n```"
                )
            )
    bot.logger.error(f"An error occurred while processing {title}:\n{tb}")
    await log_error(bot, type(error).__name__, tb)