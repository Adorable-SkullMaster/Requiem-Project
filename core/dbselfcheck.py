import asyncpg

async def dbselfcheck(bot, prefix):
    async with bot.pool.acquire() as conn:
        bot.logger.info("Checking Database Integrity...")
        await conn.execute(
            f"""
        CREATE TABLE IF NOT EXISTS guilds(
            guild BIGINT,
            prefix TEXT default '{prefix}',
            greetchan BIGINT default 0,
            greetmsg TEXT default '0',
            greetembed BOOLEAN default True,
            leavechan BIGINT default 0,
            leavemsg TEXT default '0',
            leaveembed BOOLEAN default True,
            invblk BIGINT default 0,
            autorole BIGINT default 0,
            logs BIGINT default 0,
            pwaa INT default 0,
            pwkey TEXT default '0',
            warmonitor BIGINT default 0,
            applicantmonitor BIGINT default 0,
            applicants TEXT default '0',
            pwrole BIGINT default 0,
            henchan BIGINT default 0,
            hentime INT default 0,
            hennext INT default 0,
            mmrvalues TEXT default '0',
            mmrcities TEXT default '0',
            muterole TEXT default '0')"""
        )
        await conn.execute(
            f"""
        CREATE TABLE IF NOT EXISTS disreg(
            discordid BIGINT,
            nation INT)"""
        )
        data = await conn.fetch(
            "SELECT guild FROM guilds"
        )
        output = "No Faults Found in Database Entries!"
        guilds = [guild.id for guild in bot.guilds]
        stored = [row[0] for row in data]
        for guild in bot.guilds:
            if guild.id not in stored:
                output = (
                    "Missing or invalid database entries found! Cleaning the tables..."
                )
                await conn.execute("INSERT INTO guilds VALUES(($1))", guild.id)
        for row in data:
            if row[0] not in guilds:
                output = (
                    "Missing or invalid database entries found! Cleaning the tables..."
                )
                await conn.execute(f"DELETE FROM guilds WHERE guild = ($1)", row[0])
        bot.logger.info(output)