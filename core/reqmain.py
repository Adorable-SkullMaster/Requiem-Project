import sys

sys.path.insert(0, '../')
from discord.ext import commands
from core.resourcehandler import rsshandle
from core.dbselfcheck import dbselfcheck
from core.modulehandler import modulehandler
import os
import discord
import aiohttp
import logging
import asyncpg
import json
import time


def credentials():
    with open("../credentials.json") as credentials:
        data = json.load(credentials)
    return data


async def prefix(bot, message):
    try:
        async with bot.pool.acquire() as conn:
            result = await conn.fetchval(
                "SELECT prefix FROM guilds WHERE guild = ($1)", message.guild.id
            )
            return result
    except:
        return credentials()["default_prefix"]


# Client Details & CSESS
bot = commands.Bot(
    description="A bot with tools and information for various different games",
    command_prefix=prefix,
)
bot.reqver = rsshandle("version.txt")
bot.csess = aiohttp.ClientSession(loop=bot.loop)
bot.remove_command("help")
bot.starttime = time.monotonic()
bot.er = 0000
bot.dbconn = False

# Event Logger
bot.logger = logging.getLogger("discord")
bot.logger.setLevel(logging.INFO)
FORMAT = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)
stream_handler.setFormatter(FORMAT)
bot.logger.addHandler(stream_handler)


@bot.event
async def on_ready():
    if bot.dbconn is False:
        bot.pool = await asyncpg.create_pool(
            database=credentials()["dbname"],
            host="localhost",
            user=credentials()["dbuser"],
            password=credentials()["dbpass"],
        )
        bot.dbconn = True
        output = "A new connection pool has been created!"
    else:
        output = "A connection pool already exists! Proceeding..."
    bot.logger.info(output)
    await dbselfcheck(bot, credentials()['default_prefix'])
    await modulehandler(bot, 'start')
    if not os.path.exists('data'):
        bot.logger.warning('Missing Data Directory! Creating...')
        os.makedirs('data')
    try:
        data = rsshandle('bootcheck.json')
        if data[0] != 0:
            if data[0] != 1:
                bot.logger.info("Reboot Complete! Starting...")
                channel = bot.get_channel(data[0])
                message = await channel.get_message(data[1])
                await message.edit(
                    embed=discord.Embed(title="Reboot Finished!", color=0xc71a)
                )
                if data[2] == 1:
                    check = [0, 0, 1]
                else:
                    check = [0, 0, 0]
                rsshandle('bootcheck.json', check)
    except Exception as e:
        chk = [0, 0, 0]
        rsshandle('bootcheck.json', chk)


@bot.check
async def botcheck(ctx):
    return not ctx.message.author.bot


bot.run(credentials()["api key"])
