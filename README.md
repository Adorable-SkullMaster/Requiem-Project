
# Requiem-Project

[![mit](https://img.shields.io/badge/Licensed%20under-MIT-red.svg?style=flat-square)](./LICENSE)
[![made-with-python](https://img.shields.io/badge/Made%20with-Python%203.6.5-ffde57.svg?&colorB=4584b6&style=flat-square&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAPBSURBVFhHtVdNaBNBFJ5Nqz3VRsWDF1E8iaCNFhKFaquCBxFT7EFBaQOCKLW0HixUsL15KVVEwYNIC+qhJf0JKoJgCxabUMFUPYlie1E8JejBmjYzfm9nf2aTze4i8aXfvjfvvXnfy2RnZ6uxgBLrTzUzJvYzzluFxnYxwbcwwcKMcbhFAfgGI4t4avFh78hK7ocwpnqKbwOx66kwiHpQ/CpI6gUG0BKS3LBVP2nemRk+P0o1vCRk6MoiyQdQ8Df0fcaLH/zJdYxEe0fjVMJL/BtgYq8szC6mb7Zdgj7kT464tDtgeEqAFeAohY/gO43x1nJC1bbISW/HxVN8G5B1qCAbivaNz2D0xkmo2gq5FfOWAD+BsdxStwC481UC0hQrIYctdO0tARpgCRSLoF4EtoRuw6f7ed5upJSc4C3WNoz1T2O78dOY02xtNV2bK2D4yu0zQJ0a08mlnX1+bqx9c3jdgoxzVlwTczUhMaztfjmLBNkAyHGziHHMbbLI9QIGuSxWYrs3ZpNTnJkNfLZyDBRWi+11kVdJ8ye4heQqkxtazVGwvkZ7IN4fCYdi/VNxOOJVJ/cDw80sRGcIc9v+C7nMWdrcUFtv+Uy/MR/zWkK47LGDVSUnPQp9uMxvQEMMK1DcJINVJc/jMpi5PDUL+6jiL0VYi/YlYfmSU8F30F+hf5px+pg2LvjTc5c1oT1Jd03giGYDcPRYOS5AA+P0VaTDvWAKg470lQnynYR7h5XjyLWAVRXbgDYM6EZTY2XQotfGcjAqPF7FEgaRdPdkJ2wcyUZBJ6E7guQA1C0tr+20yaGKj9PdE3QcDwJVJwe+owGeshwKOdmaFnrLi+I4xg16zK9wkByCmcdEhp6E8jmtT7TJSfO1P79C9P4XtKibvxRqHhPJUGbo7CPsApxoTvIyuxIo51/IhVjE8Kk8C+iOLU0wba/iXjEVlOfMza6uiYQWTeet4xgvkI0I0J3eSMn0kOHFwoWFnmddGNB5oRbADMt+4fCbcG+Obvp5BG8TOfH6vpaLTycmMcnZgFI8ly8c2LihdsURt5HXmuZoK1cU/zcitSARl3wzkM9D01PSBj01JSb1Gh4SvAH3JXWC6c+UrNPnLcEa8CO34iyh7XuN90SRsGPeEqSBJauYG0xyqfX3POgvdtxbfBvAu9s9q1gpylaGz4iFg7ST7sgxm5ZVKovvLiARH4/R/wO0G4KeB4ILcRff7oYWy+jbrZIEasAUvtgax4RTIMAzg1Mz6nG7jL5yMOgmTGKfzxnTPISxv/JfZTs2HsqNAAAAAElFTkSuQmCC
)](https://www.python.org/)
[![Discord](https://img.shields.io/discord/265828729970753537.svg?logo=discord&label=Sebi%27s+Bot+Tutorial&style=flat-square)](https://discord.gg/GWdhBSp)

PLEASE ENSURE YOU HAVE PYTHON 3.5 OR NEWER, POSTGRESQL, AND GITBASH INSTALLED BEFORE ATTEMPTING TO INSTALL REQUIEMS DEPENDENCIES!

WARNING!!! REQUIEM IS VERY POORLY OPTIMIZED! IT WILL CHEW THROUGH WEAKER CPUS! DO NOT 
ATTEMPT TO HOST REQUIEM IF YOUR COMPUTER IS NOT CAPABLE OF HANDLING THE LOAD AND THE
SUBSEQUENT HEAT THAT RUNNING AT HIGH CPU USAGE CAN GENERATE!!!

Requiem comes with install scripts for both Linux and Windows! The linux installer is 
experimental! Please keep this in mind and report any issues to the Requiem GITLAB repo!

To begin, start by opening Windows Powershell in administrative mode.
Do this by clicking start and typing "windows powershell".
Right click the first item to appear and click "run as administrator"

Once open, navigate to the location where you placed the reqiuem project folder.
Type .\install.ps1

The script will enable your system to run scripts (this is off by default on windows)
as well as install any dependcies the bot may need and create its database using the information
you provide. It will also place a credentials.json file in the requiem folder! Keep in mind that 
if you do not have the required prerequisites installed, the installation will fail and/or the 
bot may refuse to start!
