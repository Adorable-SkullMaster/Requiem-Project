
Set-ExecutionPolicy RemoteSigned
python -m pip install -U virtualenv
python -m venv reqvenv
Set-Location reqvenv\scripts
.\activate.ps1
python -m pip install -U git+https://github.com/Rapptz/discord.py@rewrite#egg=discord.py[voice]
python -m pip install asyncpg
python -m pip install --upgrade aiohttp
python -m pip install psutil
python -m pip install imgurpython
python -m pip install bs4
python -m pip install pycparser

Set-Location ..\..
python setup.py