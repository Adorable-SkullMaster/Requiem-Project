import json


def json_unfuckifier(data: str, max_loops=None, **kwargs):
    if max_loops is None:
        max_loops = -1

    loop = 0
    last_error = None
    while loop != max_loops:
        loop += 1
        try:
            return json.loads(data, **kwargs)
        except json.JSONDecodeError as ex:
            last_error = ex
            # Explode
            index = ex.pos
            data = list(ex.doc)

            if data[index - 1] == ',':
                while index < len(data) and data[index] in '\n\r\t, ':
                    data.pop(index)
            elif str(ex).startswith('Expecting property name enclosed in double quotes'):
                index = len(data) - index
                data = list(reversed(data))
                while data[index] in '\r\n\t, ':
                    data.pop(index)
                data = ''.join(reversed(data))
            else:
                raise RuntimeError(f'I do not know how to deal with {str(ex)!r}')

            data = ''.join(data)
            print('Recursing', data)

    error = RuntimeError('Could not fix malformed JSON in given number of passes.')
    raise error from last_error