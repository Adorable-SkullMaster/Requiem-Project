
import discord
from discord.ext import commands
from src.globalFunctions import randColour

import asyncio
import itertools
import sys
import traceback
from async_timeout import timeout
from functools import partial
from youtube_dl import YoutubeDL


ytdlopts = {
    'format': 'bestaudio/best',
    'outtmpl': 'downloads/%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': False,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0'  # ipv6 addresses cause issues sometimes
}

ffmpegopts = {
    'before_options': '-nostdin',
    'options': '-vn'
}

ytdl = YoutubeDL(ytdlopts)


class VoiceConnectionError(commands.CommandError):
    """Custom Exception class for connection errors."""


class InvalidVoiceChannel(VoiceConnectionError):
    """Exception for cases of invalid Voice Channels."""


class YTDLSource(discord.PCMVolumeTransformer):

    def __init__(self, source, *, data, requester):
        super().__init__(source)
        self.requester = requester

        self.title = data.get('title')
        self.web_url = data.get('webpage_url')

        # YTDL info dicts (data) have other useful information you might want
        # https://github.com/rg3/youtube-dl/blob/master/README.md

    def __getitem__(self, item: str):
        """Allows us to access attributes similar to a dict.

        This is only useful when you are NOT downloading.
        """
        return self.__getattribute__(item)

    @classmethod
    async def create_source(cls, ctx, search: str, *, loop, download=False):
        loop = loop or asyncio.get_event_loop()

        to_run = partial(ytdl.extract_info, url=search, download=download)
        data = await loop.run_in_executor(None, to_run)

        if 'entries' in data:
            # take first item from a playlist
            data = data['entries'][0]

        if download:
            source = ytdl.prepare_filename(data)
        else:
            return {'webpage_url': data['webpage_url'], 'requester': ctx.author, 'title': data['title']}

        return cls(discord.FFmpegPCMAudio(source), data=data, requester=ctx.author)

    @classmethod
    async def regather_stream(cls, data, *, loop):
        """Used for preparing a stream, instead of downloading.

        Since Youtube Streaming links expire."""
        loop = loop or asyncio.get_event_loop()
        requester = data['requester']

        to_run = partial(ytdl.extract_info, url=data['webpage_url'], download=False)
        data = await loop.run_in_executor(None, to_run)

        return cls(discord.FFmpegPCMAudio(data['url']), data=data, requester=requester)


class MusicPlayer:
    """A class which is assigned to each guild using the bot for Music.

    This class implements a queue and loop, which allows for different guilds to listen to different playlists
    simultaneously.

    When the bot disconnects from the Voice it's instance will be destroyed.
    """

    __slots__ = ('bot', '_guild', '_channel', '_cog', 'queue', 'next', 'current', 'np', 'volume')

    def __init__(self, ctx):
        self.bot = ctx.bot
        self._guild = ctx.guild
        self._channel = ctx.channel
        self._cog = ctx.cog

        self.queue = asyncio.Queue()
        self.next = asyncio.Event()

        self.np = None  # Now playing message
        self.volume = .5
        self.current = None

        ctx.bot.loop.create_task(self.player_loop())

    async def player_loop(self):
        """Our main player loop."""
        await self.bot.wait_until_ready()

        while not self.bot.is_closed():
            self.next.clear()

            try:
                # Wait for the next song. If we timeout cancel the player and disconnect...
                async with timeout(300):  # 5 minutes...
                    source = await self.queue.get()
            except asyncio.TimeoutError:
                return self.destroy(self._guild)

            if not isinstance(source, YTDLSource):
                # Source was probably a stream (not downloaded)
                # So we should regather to prevent stream expiration
                try:
                    source = await YTDLSource.regather_stream(source, loop=self.bot.loop)
                except Exception as e:
                    await self._channel.send(f'There was an error processing your song.\n'
                                             f'```css\n[{e}]\n```')
                    continue

            source.volume = self.volume
            self.current = source

            self._guild.voice_client.play(source, after=lambda _: self.bot.loop.call_soon_threadsafe(self.next.set))
            embed=discord.Embed(
                description=f"Now Playing: {source.title}\nRequested By: {source.requester}",
                color=randColour()
            )
            self.np = await self._channel.send(embed=embed)
            await self.next.wait()

            # Make sure the FFmpeg process is cleaned up.
            source.cleanup()
            self.current = None

            try:
                # We are no longer playing this song...
                await self.np.delete()
            except discord.HTTPException:
                pass

    def destroy(self, guild):
        """Disconnect and cleanup the player."""
        return self.bot.loop.create_task(self._cog.cleanup(guild))


class music:
    """Music related commands."""

    __slots__ = ('bot', 'players')

    def __init__(self, bot):
        self.bot = bot
        self.players = {}

    async def cleanup(self, guild):
        try:
            await guild.voice_client.disconnect()
        except AttributeError:
            pass

        try:
            del self.players[guild.id]
        except KeyError:
            pass

    async def __local_check(self, ctx):
        """A local check which applies to all commands in this cog."""
        if not ctx.guild:
            raise commands.NoPrivateMessage
        return True

    async def __error(self, ctx, error):
        """A local error handler for all errors arising from commands in this cog."""
        if isinstance(error, commands.NoPrivateMessage):
            try:
                return await ctx.send(embed=discord.Embed(description='This command can not be used in Private Messages.', color=randColour()))
            except discord.HTTPException:
                pass
        elif isinstance(error, InvalidVoiceChannel):
            await ctx.send('Error connecting to Voice Channel. '
                           'Please make sure you are in a valid channel or provide me with one')

        print('Ignoring exception in command {}:'.format(ctx.command), file=sys.stderr)
        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    def get_player(self, ctx):
        """Retrieve the guild player, or generate one."""
        try:
            player = self.players[ctx.guild.id]
        except KeyError:
            player = MusicPlayer(ctx)
            self.players[ctx.guild.id] = player

        return player

    @commands.command(name='connect', aliases=['join'])
    async def connect_(self, ctx, *, channel: discord.VoiceChannel=None):
        """Connect to voice.

        Parameters
        ------------
        channel: discord.VoiceChannel [Optional]
            The channel to connect to. If a channel is not specified, an attempt to join the voice channel you are in
            will be made.

        This command also handles moving the bot to different channels.
        """
        if not channel:
            try:
                channel = ctx.author.voice.channel
            except AttributeError:
                raise InvalidVoiceChannel('No channel to join. Please either specify a valid channel or join one.')

        vc = ctx.voice_client

        if vc:
            if vc.channel.id == channel.id:
                return
            try:
                await vc.move_to(channel)
            except asyncio.TimeoutError:
                raise VoiceConnectionError(f'Moving to channel: <{channel}> timed out.')
        else:
            try:
                await channel.connect()
            except asyncio.TimeoutError:
                raise VoiceConnectionError(f'Connecting to channel: <{channel}> timed out.')

        await ctx.send(embed=discord.Embed(description=f'Connected to: {channel}', color=randColour()), delete_after=20)

    @commands.command(name='play', aliases=['sing'])
    async def play_(self, ctx, *, search: str):
        """Request a song and add it to the queue.

        This command attempts to join a valid voice channel if the bot is not already in one.
        Uses YTDL to automatically search and retrieve a song.

        Parameters
        ------------
        search: str [Required]
            The song to search and retrieve using YTDL. This could be a simple search, an ID or URL.
        """
        await ctx.message.delete()
        async with ctx.typing():
            try:
                vc = ctx.voice_client

                if not vc:
                    await ctx.invoke(self.connect_)

                player = self.get_player(ctx)

                # If download is False, source will be a dict which will be used later to regather the stream.
                # If download is True, source will be a discord.FFmpegPCMAudio with a VolumeTransformer.
                source = await YTDLSource.create_source(ctx, search, loop=self.bot.loop, download=False)

                if "ear" and "rape" not in source["title"].lower():
                    embed = discord.Embed(
                        description=f"Added {source['title']} to the Queue!",
                        color=randColour()
                    )
                    await ctx.send(embed=embed, delete_after=15)
                    return await player.queue.put(source)
                embed=discord.Embed(
                    description="""
Nobody appreciates "ear rape", least of all headphone users! 
Please have a little bit of maturity when picking what to play!""",
                    color=randColour()
                )
                await ctx.send(embed=embed, delete_after=15)
            except Exception as e:
                print(e)
                embed=discord.Embed(
                    description="I was not able to find that video!",
                    color=randColour()
                )
                await ctx.send(embed=embed)

    @commands.command(name='pause')
    async def pause_(self, ctx):
        """Pause the currently playing song."""
        vc = ctx.voice_client

        if not vc or not vc.is_playing():
            return await ctx.send(embed=discord.Embed(description='I am not currently playing anything!', color=randColour()), delete_after=20)
        elif vc.is_paused():
            return

        vc.pause()
        await ctx.send(embed=discord.Embed(description=f'{ctx.author}: Paused the song!', color=randColour()))

    @commands.command(name='resume')
    async def resume_(self, ctx):
        """Resume the currently paused song."""
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            return await ctx.send(embed=discord.Embed(description='I am not currently playing anything!', color=randColour()), delete_after=20)
        elif not vc.is_paused():
            return

        vc.resume()
        await ctx.send(embed=discord.Embed(description=f'{ctx.author}: Resumed the song!', color=randColour()))

    @commands.command(name='forceskip')
    @commands.has_permissions(manage_guild=True)
    async def force_skip(self, ctx):
        """Force skips the song! Requires "manage_guild" perms!"""
        vc = ctx.voice_client
        if not vc or not vc.is_connected():
            embed = discord.Embed(
                title="Music Player",
                description="I am not currently playing anything!",
                color=randColour()
            )
            return await ctx.send(embed=embed, delete_after=20)
        if vc.is_paused():
            pass
        elif not vc.is_playing():
            return

        vc.stop()

    @commands.command(name='skip')
    async def skip_(self, ctx):
        """Skip the song."""
        vc = ctx.voice_client
        if not vc or not vc.is_connected():
            embed = discord.Embed(
                title="Music Player",
                description="I am not currently playing anything!",
                color=randColour()
            )
            return await ctx.send(embed=embed, delete_after=20)
        if vc.is_paused():
            pass
        elif not vc.is_playing():
            return

        def stop():
            vc.stop()

        if len(vc.channel.members) > 2:
            embed = discord.Embed(
                title="Music Player",
                description=f"""
{ctx.author.mention} has requested the current song be skipped!
If a majority vote is reached, I will skip this track!""",
                color=randColour()
            )
            msg = await ctx.send(embed=embed)
            await msg.add_reaction("✅")
            await asyncio.sleep(1)
            await msg.add_reaction("❎")
            await asyncio.sleep(1)
            pro = 0
            against = 0
            total = len(vc.channel.members) - 1

            def check(r, u):
                return u in vc.channel.members

            try:
                for i in range(120):
                    reaction, user = await self.bot.wait_for("reaction_add", check=check)
                    if pro >= total * .75:
                        stop()
                        break
                    elif against >= total * .75:
                        raise Exception
                    elif str(reaction.emoji) == "✅":
                        pro = pro + 1
                    elif str(reaction.emoji) == "❎":
                        against = against + 1
                    await asyncio.sleep(1)
                emebd = discord.Embed(
                    title="Music Player",
                    description="A majority was reached! Skipping...",
                    color=randColour()
                )
            except Exception as e:
                embed = discord.Embed(
                    description="A majority vote was not reached!",
                    color=randColour()
                )
            await msg.edit(embed=embed, delete_after=20)
        else:
            embed = discord.Embed(
                description="The song has been skipped!",
                color=randColour()
            )
            await ctx.send(embed=embed, delete_after=20)
            stop()

    @commands.command(name='queue', aliases=['q'])
    async def queue_info(self, ctx):
        """Retrieve a basic queue of upcoming songs."""
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            return await ctx.send(embed=discord.Embed(description='I am not currently connected to voice!', color=randColour()), delete_after=20)

        player = self.get_player(ctx)
        if player.queue.empty():
            return await ctx.send(embed=discord.Embed(description='There are currently no more queued songs.', color=randColour()))

        output = ""
        pages = []
        for i in player.queue._queue:
            if len(output) < 1000:
                output = output + i["title"] + "\n\n"
            else:
                pages.append(output)
                output = ""
                output = output + i["title"] + "\n\n"
        if len(pages) == 0:
            pages.append(output)

        pag = 0
        def que(pag):
            return discord.Embed(
                title=f'In Queue - {len(player.queue._queue)}',
                description=f"```{pages[pag]}```",
                color=randColour()
            )
        message = await ctx.send(embed=que(pag))

        if len(pages) > 1:
            await message.add_reaction('⬅')
            await asyncio.sleep(1)
            await message.add_reaction('➡')
            await asyncio.sleep(1)
            for i in range(120):
                try:
                    reaction, user = await self.bot.wait_for("reaction_add")
                    if str(reaction.emoji) == '➡' and pag < len(pages):
                        pag = pag + 1
                    elif str(reaction.emoji) == '⬅' and pag > 0:
                        pag = pag - 1
                    await message.edit(embed=que(pag))
                    await message.remove_reaction(str(reaction.emoji), user)
                    await asyncio.sleep(1)
                except:
                    pass





    @commands.command(name='now_playing', aliases=['np'])
    async def now_playing_(self, ctx):
        """Display information about the currently playing song."""
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            return await ctx.send(embed=discord.Embed(description='I am not currently connected to voice!', color=randColour()), delete_after=20)

        player = self.get_player(ctx)
        if not player.current:
            return await ctx.send(embed=discord.Embed(description='I am not currently playing anything!', color=randColour()))

        try:
            # Remove our previous now_playing message.
            await player.np.delete()
        except discord.HTTPException:
            pass

        embed=discord.Embed(description=f"Now Playing: {vc.source.title}\nRequested By: {vc.source.requester}")
        player.np = await ctx.send(embed=embed)

    @commands.command(name='volume', aliases=['vol'])
    async def change_volume(self, ctx, *, vol: float):
        """Change the player volume.

        Parameters
        ------------
        volume: float or int [Required]
            The volume to set the player to in percentage. This must be between 1 and 100.
        """
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            return await ctx.send(embed=discord.Embed(description='I am not currently connected to voice!', color=randColour()), delete_after=20)

        if not 0 < vol < 101:
            return await ctx.send(embed=discord.Embed(description='Please enter a value between 1 and 100.', color=randColour()), delete_after=20)

        player = self.get_player(ctx)

        if vc.source:
            vc.source.volume = vol / 100

        player.volume = vol / 100
        await ctx.send(embed=discord.Embed(description=f'{ctx.author}: Set the volume to {vol}%', color=randColour()))

    @commands.command(name='stop')
    async def stop_(self, ctx):
        """Stop the currently playing song and destroy the player.

        !Warning!
            This will destroy the player assigned to your guild, also deleting any queued songs and settings.
        """
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            return await ctx.send(embed=discord.Embed(description='I am not currently playing anything!', color=randColour()), delete_after=20)

        await self.cleanup(ctx.guild)


def setup(bot):
    bot.add_cog(music(bot))
