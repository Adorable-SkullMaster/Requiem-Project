
import platform
import discord
from discord.ext import commands
import time
from core.resourcehandler import rsshandle
from src.globalFunctions import randColour

class utility:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(brief="Displays the bots latency")
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def ping(self, ctx):
        """Displays the bot's latency."""
        ping_time = int(self.bot.latency * 1000)
        if ping_time <= 70:
            pcolor = 0xc71a
        elif ping_time <= 140:
            pcolor = 0xf8e71c
        elif ping_time >= 141:
            pcolor = 0xe40000
        await ctx.send(
            embed=discord.Embed(title=f"**Pong!, took {ping_time}ms.**", color=pcolor)
        )

    @commands.command(brief="Provides information on the specified user")
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def user(self, ctx, user: discord.Member = None):
        """Provides information about a specified user. Leave blank to receive  information about your account."""
        if user is None:
            user = ctx.message.author
        roles = ", ".join(i.name for i in user.roles[1:])
        embed = discord.Embed(title=user.name, color=randColour())
        if user.name != user.display_name:
            embed.add_field(name="Nickname:", value=user.display_name)
        embed.add_field(name="ID:", value=user.id)
        embed.add_field(
            name="Account Created On:",
            value=f'{user.created_at.strftime("%A %B %d, %Y")}',
        )
        embed.add_field(
            name="Member Joined On:",
            value=f'{user.joined_at.strftime("%A %B %d, %Y")}',
        )
        embed.add_field(name="Roles", value=roles, inline=False)
        try:
            embed.add_field(name=user.activity.type.name.title(), value=user.activity.name)
        except:
            pass
        embed.add_field(name="**Status:**", value=str(user.status).upper())
        embed.set_thumbnail(url=user.avatar_url)
        await ctx.send(embed=embed)

    @commands.command(aliases=["server"], brief="Provides information about this guild")
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def guild(self, ctx):
        """Provides information about this guild."""
        guild = ctx.guild
        embed = discord.Embed(title=guild.name, color=randColour())
        embed.add_field(name="Guild Owner", value=guild.owner)
        embed.add_field(name="Guild ID", value=guild.id)
        embed.add_field(name="Region", value=guild.region)
        embed.add_field(
            name="Server Verification Level",
            value=guild.verification_level,
        )
        embed.add_field(
            name="Server Created On",
            value=guild.created_at.strftime("%A %B %d, %Y"),
        )
        embed.add_field(name="Members", value=guild.member_count)
        embed.add_field(name="Roles", value=len(guild.roles))
        embed.add_field(
            name="Text Channels", value=len(guild.text_channels)
        )
        embed.add_field(
            name="Voice Channels", value=len(guild.voice_channels)
        )
        embed.set_thumbnail(url=guild.icon_url)
        await ctx.send(embed=embed)

    @commands.command(brief="Gives avatar image for the specified user")
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def avatar(self, ctx, user: discord.Member = None):
        """Gives avatar image for the specified user."""
        if user is None:
            user = ctx.author
        embed = discord.Embed(title=f"{user.name}'s Profile Picture", color=randColour())
        embed.set_image(url=user.avatar_url)
        await ctx.send(embed=embed)

    @commands.command(brief="Displays general information about Requiem")
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def botinfo(self, ctx):
        """Displays general information about the bot, such as version info and contributors"""
        total_seconds = time.monotonic() - self.bot.starttime
        mins, secs = divmod(total_seconds, 60)
        hours, mins = divmod(mins, 60)
        uptime = f"{int(hours)} Hours {int(mins)} Minutes {int(secs)} Seconds"
        output = rsshandle("botinfo.txt")
        output = output.format(
            self.bot.reqver, discord.__version__, platform.python_version(), uptime,
            len(ctx.bot.guilds), len(ctx.bot.users)
        )
        embed = discord.Embed(
            title="General Information",
            color=randColour(),
            description=output
        )
        embed.set_thumbnail(url="https://imgur.com/3NPQXDE.gif")
        await ctx.send(embed=embed)

    @commands.command(brief="Lists changes in the lastest patch")
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def update(self, ctx, *, updatefile=None):
        """Provides the information about the latest patch. Provide argument file to get a full updatelog!"""
        if updatefile == "full":
            await ctx.send(file=discord.File("data/updatelog.txt"))
        else:
            embed = discord.Embed(
                title=f"Requiem Version: {self.bot.reqver}",
                color=randColour(),
                description="""
>added a music module
>>there were a few modifications to the music module, 
its repo can be found by using the botinfo command!
>added randColour
>fixed help not showing aliases, again
>made rsshandle a function instead of a coru
>fixed activity background task
>fixed bot sending dev branch warning twice
>added konachan to nsfw_cache
"""
            )
            await ctx.send(embed=embed)

    @commands.command(brief="Provides a bot invite link")
    async def botinvite(self, ctx):
        """Provides a link that can be used to add the bot to a guild!
         In order to add bots to a guild, you must have manage_guild permissions!"""
        embed = discord.Embed(
            title="You can use this link to invite the bot to your guild!",
            description=f"""
            <https://discordapp.com/oauth2/authorize?client_id={self.bot.user.id}&scope=bot&permissions=805317758>
            """,
            color=randColour(),
        )
        await ctx.send(embed=embed)

    @commands.command(brief="Provides the link to my github repository")
    async def git(self, ctx):
        """Provides the link to my gitlab repository!"""
        embed = discord.Embed(
            title="Requiems Github Repository",
            description="https://gitlab.com/AnakiKaiver297/Requiem-Project",
            color=randColour(),
        )
        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(utility(bot))
