
from discord.ext import commands
import discord
import asyncio
import aiohttp
import json
import random


class nsfw:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(brief="Sends some hentai")
    @commands.is_nsfw()
    async def konachan(self, ctx, tag=None):
        """Sends some hentai. NOT SAFE FOR WORK! Requires NSFW channel."""
        if tag == None:
            tag = "sex"
        else:
            tag = tag + "%20sex"
        choices = []
        async with self.bot.csess.get(
            f"https://konachan.com/post.json?tags={tag}"
        ) as response:
            data = await response.json(content_type="application/json")
        for I in data:
            choices.append(I["file_url"])
        await ctx.send(
            embed=discord.Embed(color=0x9013fe).set_image(url=random.choice(choices))
        )

    @commands.command(brief="Enables/disables auto-hentai feature")
    @commands.is_nsfw()
    @commands.has_permissions(manage_guild=True)
    async def autohentai(self, ctx, timer: int = None):
        """Enables/disables the auto-hentai feature! Requires an NSFW channel and the MANAGE_GUILD permissions!"""
        async with self.bot.pool.acquire() as conn:
            hentime = await conn.fetchval(
                "SELECT hentime FROM guilds WHERE guild = ($1)", ctx.guild.id
            )
            henchan = ctx.channel.id
            if timer is None:
                henchan = 0
                timer = 0
                output = "Auto hentai is now disabled"
            elif timer >= 20:
                if hentime is not 0:
                    output = f"Auto hentai timer updated! Timer: {timer} seconds"
                else:
                    output = f"Auto hentai is now enabled! Timer: {timer} seconds"
            else:
                timer = 0
                henchan = 0
                output = f"Auto hentai timer cannot be set to less than 20 seconds!"
            await conn.execute(
                "UPDATE guilds SET hentime = ($1), henchan = ($2) WHERE guild = ($3)",
                timer,
                henchan,
                ctx.guild.id,
            )
            await ctx.send(embed=discord.Embed(title=output, color=0x9013fe))


def setup(bot):
    bot.add_cog(nsfw(bot))
