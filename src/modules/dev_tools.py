import json
import logging
import random
import discord
from discord.ext import commands
from core.modulehandler import modulehandler
from core.resourcehandler import rsshandle
import asyncio
import os


class dev_tools:
    def __init__(self, bot):
        self.bot = bot

    @commands.is_owner()
    @commands.command(pass_context=True)
    async def avt(self, ctx, genre, *, url):
        """used by dev to add urls to the 'gimmenightcore' command"""
        message = ctx.message
        await message.delete()
        with open("data/urldirectory.txt") as urldir:
            data = json.load(urldir)
            sourcelist = []
            urllist = []
            for I in data:
                sourcelist.append(I)
            for I in data[f"{genre}"]:
                urllist.append(I)
            if genre in sourcelist:
                if url.startswith("https://www.youtube.com/watch?v="):
                    if url in urllist:
                        await ctx.send(
                            "The url you have provided is already in the dict!"
                        )
                    else:
                        data[f"{genre}"].append(url)
                        await ctx.send("Success!")
                        logging.info(f"{url} added to the dict!")
                        with open("data/urldirectory.txt", "w") as urldir:
                            json.dump(data, urldir)
                else:
                    await ctx.send("The url you have provided is not a youtube url")
            else:
                await ctx.send("Please provide a valid genre!")

    @commands.command(hidden=True)
    @commands.is_owner()
    async def error(self, ctx, dis: bool = False):
        """Tests error handling."""
        if dis:
            from discord.ext.commands import errors

            error_type = random.choice(
                (
                    errors.TooManyArguments,
                    errors.CommandOnCooldown,
                    errors.DisabledCommand,
                    errors.CommandNotFound,
                    errors.NoPrivateMessage,
                    errors.MissingPermissions,
                    errors.NotOwner,
                    errors.BadArgument,
                    errors.MissingRequiredArgument,
                    errors.CheckFailure,
                    errors.CommandError,
                    errors.DiscordException,
                    errors.CommandInvokeError,
                )
            )
        else:
            error_type = random.choice(
                (
                    Exception,
                    RuntimeError,
                    IOError,
                    BlockingIOError,
                    UnboundLocalError,
                    UnicodeDecodeError,
                    SyntaxError,
                    SystemError,
                    NotImplementedError,
                    FileExistsError,
                    FileNotFoundError,
                    InterruptedError,
                    EOFError,
                    NameError,
                    AttributeError,
                    ValueError,
                    KeyError,
                    FutureWarning,
                    DeprecationWarning,
                    PendingDeprecationWarning,
                )
            )

        await ctx.send(
            embed=discord.Embed(
                title=f"Raising {error_type.__qualname__}", color=0xc71a
            )
        )
        raise error_type

    @commands.command()
    @commands.is_owner()
    async def clearerrors(self, ctx):
        """Clears error reports, allows for former error reports to be resubmitted"""
        data = []
        await rsshandle("errors.json", data)
        await ctx.send(
            embed=discord.Embed(
                title="All Error Reports Have Been Cleared!", color=0xc71a
            )
        )

    @commands.command()
    @commands.is_owner()
    async def clearnsfw(self, ctx):
        """Clears nsfw cache file"""
        data = []
        rsshandle("nsfw_cache.json", data)
        await ctx.send(
            embed=discord.Embed(
                title="The NSFW Cache File Has Been Cleared!", color=0xc71a
            )
        )

    @commands.command()
    @commands.is_owner()
    async def cycle(self, ctx):
        """Owner Only! Completely reloads all self.bot modules! Keep in mind that the self.bot may crash
        or get trapped in an endless loop during this process! If that occures
        a manual restart will be required!"""
        result = await modulehandler(self.bot, 'cycle')
        if result is False:
            output = "One or more modules failed to load while cycling! Refer to shell for more details!"
        else:
            output = "Module Cycling Complete! No Issues Detected!"
            embed=discord.Embed()
        embed=discord.Embed(
            title="Module Handling Service",
            description=output,
            color=0x9013fe
        )
        await ctx.send(embed=embed)

    @commands.command()
    @commands.is_owner()
    async def reload(self, ctx, *, module):
        """self.bot Owner Only! Reloads the specified module..."""
        response = await modulehandler(self.bot, module)
        if response == "BadInput":
            output = "No such module exists! Please specify a valid module name!"
        elif response is False:
            output = f"There was an issue while reloading {module}! Please refer to shell for more details!"
        else:
            output = f"{module} reloaded successfully!"
        embed = discord.Embed(
            title="Module Handling Service",
            description=output,
            color=0x9013fe
        )
        await ctx.send(embed=embed)

    @commands.command()
    @commands.is_owner()
    async def reboot(self, ctx):
        """self.bot Owner Only! Restarts the service!"""
        chk = rsshandle('bootcheck.json')
        message = await ctx.send(
            embed=discord.Embed(title="Restarting!", colour=0xc71a)
        )
        if chk[2] == 1:
            check = [ctx.channel.id, message.id, 0]
        else:
            check = [ctx.channel.id, message.id, 1]
        await self.bot.csess.close()
        rsshandle('bootcheck.json', check)
        await modulehandler(self.bot, "stop")
        await ctx.bot.logout()

    @commands.is_owner()
    @commands.command()
    async def debug(self, ctx):
        chk = rsshandle('bootcheck.json')
        if chk[2] == 1:
            chk = [0, 0, 0]
            output = "Requiem is now in NORMAL MODE! I will now submit all error reports through DMs!"
        else:
            chk = [0, 0, 1]
            output = "Requiem is now in DEBUG MODE! I will no longer submit error reports through DMs!"
        rsshandle('bootcheck.json', chk)
        embed=discord.Embed(
            title="Requiem DEBUG",
            description=output,
            color=0xc71a
        )
        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(dev_tools(bot))
