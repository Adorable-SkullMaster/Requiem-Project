import discord
from discord.ext import commands
import json
import datetime
import asyncio
from core.resourcehandler import rsshandle
from core.error_handling import raise_error


async def alliances_api(self):
    url = "https://politicsandwar.com/api/alliances/"
    async with self.bot.csess.get(url) as r:
        data = json.loads((await r.read()).decode('utf-8').replace(',,', ','))
    return data

async def alliance_api(self, id):
    async with self.bot.csess.get(
        f"https://politicsandwar.com/api/alliance/id={id}"
    ) as r:
        data = await r.json(content_type="text/html")
    return data


async def mil_audit(self, user, nation, mmr, ctx, nodm):
    bypass = mmr
    mmr = mmr.split("/")
    result = "PASSED"
    sr = "PASSED"
    tr = "PASSED"
    ar = "PASSED"
    br = "PASSED"
    cityresult = ""
    cc = 0
    check = 3
    for id in nation["cityids"]:
        city = await city_api(self, id)
        cc = cc + 1
        if cc == check:
            check = check + 3
            ending = "\n"
        else:
            ending = " | "
        if cc <= 9:
            counter = f"City{cc}:  "
        else:
            counter = f"City{cc}: "
        cityresult = (
            cityresult
            + f"{counter}{city['imp_barracks']}/{city['imp_factory']}/{city['imp_hangar']}/{city['imp_drydock']}"
            + ending
        )
    if int(city["imp_barracks"]) < int(mmr[0]):
        result = "FAILED"
    if int(city["imp_factory"]) < int(mmr[1]):
        result = "FAILED"
    if int(city["imp_hangar"]) < int(mmr[2]):
        result = "FAILED"
    if int(city["imp_drydock"]) < int(mmr[3]):
        result = "FAILED"
    if int(nation["soldiers"]) < int(mmr[0]) * 3000 * cc:
        result = "FAILED"
        sr = "FAILED"
    if int(nation["tanks"]) < int(mmr[1]) * 250 * cc:
        result = "FAILED"
        tr = "FAILED"
    if int(nation["aircraft"]) < int(mmr[2]) * 18 * cc:
        result = "FAILED"
        ar = "FAILED"
    if int(nation["ships"]) < int(mmr[3]) * 5 * cc:
        result = "FAILED"
        br = "FAILED"
    milresult = f"""
{"Soldiers:":<{15}}|{" ":<{5}}{f"{nation['soldiers']}":<{10}}|{f"{sr}":>{15}}
{"Tanks:":<{15}}|{" ":<{5}}{f"{nation['tanks']}":<{10}}|{f"{tr}":>{15}}
{"Aircraft:":<{15}}|{" ":<{5}}{f"{nation['aircraft']}":<{10}}|{f"{ar}":>{15}}
{"Ships:":<{15}}|{" ":<{5}}{f"{nation['ships']}":<{10}}|{f"{br}":>{15}}"""
    await conduct_audit(self, bypass, user, ctx, nodm, result, cityresult, milresult)


async def conduct_audit(self, mmr, user, ctx, nodm, result, cityresult, milresult):
    try:
        user = self.bot.get_user(user)
        if result == "FAILED":
            subtext = """
Please take any necessary action to increase your military standing!
Failure to comply with the MMR could result in diciplinary action!
If you have any questions about the mmr or are in need of assistance,
please don't hesitate to ask a member of your government!
"""
        else:
            subtext = """
You are currently meeting mmr expectations! Good work! If you have any
questions about the mmr or are in need of assistance, please don't
hesitate to ask a member of government
"""
        des = f"""
Attention {user.mention}!

An audit has been conducted on your nations military!
You have {result} this audit!

The current MMR for your city count is: {mmr}

Your cities are as follows:
```{cityresult}```

Your military is as follows:
```{milresult}```
{subtext}

Audit conducted on {datetime.datetime.now().strftime("%A %B %d, %Y")}
"""
        embed = discord.Embed(title="MMR Audit", description=des, color=0x9013fe)
        if nodm is True:
            await ctx.send(embed=embed)
        else:
            await user.send(embed=embed)
    except:
        pass

async def prefix(self, ctx):
    async with self.bot.pool.acquire() as conn:
        prefix = await conn.fetchval(
            "SELECT prefix FROM guilds WHERE guild = ($1)", ctx.guild.id
        )
        return prefix


async def role_check(self, ctx):
    async with self.bot.pool.acquire() as conn:
        roleid = await conn.fetchval(
            "SELECT pwrole FROM guilds WHERE guild = ($1)", ctx.guild.id
        )
        role = discord.utils.get(ctx.guild.roles, id=roleid)
        if role in ctx.author.roles[1:]:
            return True
        else:
            return False


async def grab_discord(self, ctx, target=None):
    async with self.bot.pool.acquire() as conn:
        if type(target) is int:
            nationid = await conn.fetchval(
                "SELECT nation FROM disreg WHERE discordid = ($1)", target
            )
            return nationid
        else:
            disid = await conn.fetchval(
                "SELECT discordid FROM disreg WHERE nation = ($1)", int(target)
            )
            member = self.bot.get_user(disid)
            return member


async def nation_api(self, id1, id2=None):
    url = f"http://politicsandwar.com/api/nation/id={id1}"
    async with self.bot.csess.get(url) as response:
        d1 = await response.json(content_type="text/html")
    if id2 is not None:
        url = f"http://politicsandwar.com/api/nation/id={id2}"
        async with self.bot.csess.get(url) as response:
            d2 = await response.json(content_type="text/html")
        return d1, d2
    else:
        return d1


async def nations_api(self):
    url = "https://politicsandwar.com/api/nations/"
    async with self.bot.csess.get(url) as response:
        data = await response.json(content_type="text/html")
        return data


async def city_api(self, id):
    url = f"https://politicsandwar.com/api/city/id={id}"
    async with self.bot.csess.get(url) as response:
        data = await response.json(content_type="text/html")
        return data


async def wars_api(self):
    url = f"https://politicsandwar.com/api/wars"
    async with self.bot.csess.get(url) as response:
        data = await response.json(content_type="text/html")
    return data


async def war_api(self, id):
    url = f"https://politicsandwar.com/api/war/{id}"
    async with self.bot.csess.get(url) as response:
        data = await response.json(content_type="text/html")
    return data


async def alliance_members(self, ctx, key=None, id=None):
    async with self.bot.pool.acquire() as conn:
        pwkey, pwaa = await conn.fetchrow(
            f"SELECT pwkey, pwaa FROM guilds WHERE guild = {ctx.guild.id}"
        )
        if key is not None:
            pwaa = id
            pwkey = key
        url = f"http://politicsandwar.com/api/alliance-members/?allianceid={pwaa}&key={pwkey}"
        async with self.bot.csess.get(url) as response:
            data = await response.json(content_type="application/json")
            return data


async def bank_api(self, ctx):
    async with self.bot.pool.acquire() as conn:
        pwkey, pwaa = await conn.fetchrow(
            f"SELECT pwkey, pwaa FROM guilds WHERE guild = {ctx.guild.id}"
        )
        url = f"http://politicsandwar.com/api/alliance-bank/?allianceid={pwaa}&key={pwkey}"
        async with self.bot.csess.get(url) as response:
            data = await response.json(content_type="application/json")
            return data


async def stored_id(self, ctx):
    async with self.bot.pool.acquire() as conn:
        pwaa = await conn.fetchval(
            f"SELECT pwaa FROM guilds WHERE guild = {ctx.guild.id}"
        )
        return pwaa


async def trade_api(self, I):
    async with self.bot.csess.get(
        f"https://politicsandwar.com/api/tradeprice/resource={I}"
    ) as response:
        data = await response.json(content_type="text/html")
    return data


async def applicants(self, guild):
    async with self.bot.pool.acquire() as conn:
        pwkey, pwaa = await conn.fetchrow(
            f"SELECT pwkey, pwaa FROM guilds WHERE guild = {guild}"
        )
        url = f"http://politicsandwar.com/api/applicants/?allianceid={pwaa}&key={pwkey}"
        async with self.bot.csess.get(url) as response:
            data = await response.json(content_type="text/html")
            return data


class politics_and_war:
    def __init__(self, bot):
        self.bot = bot


    @commands.command(brief="Conducts an audit of all member nations")
    async def audit(self, ctx, nodm: bool = None):
        """Conducts an MMR audit on all member nations. Include "True" as a command parameter to send the audit in the
        current channel (THIS WILL SPAM THIS CHANNEL!)."""
        if await role_check(self, ctx) == True:
            async with self.bot.pool.acquire() as conn:
                prefix, pwaa, pwkey, mmrs, cities = await conn.fetchrow(
                    "SELECT prefix, pwaa, pwkey, mmrvalues, mmrcities FROM guilds WHERE guild = ($1)", ctx.guild.id
                )
            output = f"""
This guild is not properly configured to work with the PW api!
Please use "{prefix}pw config "<alliance id> [api key]" to configure this guild!"""
            count = 0
            if pwaa != 0:
                if pwkey != 0:
                    if mmrs is not "0":
                        await ctx.send(embed=discord.Embed(
                            description="Please stand by! This may take a few minutes depending on the size of your alliance!",
                            color=0x9013fe
                            ))
                        async with ctx.typing():
                            output = "The audit was completed successfully!"
                            mmrs = mmrs.split(" ")
                            cities = cities.split(" ")
                            members = await alliance_members(self, ctx)
                            for cc in cities:
                                for member in members["nations"]:
                                    user = await grab_discord(self, ctx, str(member["nationid"]))
                                    nation = await nation_api(self, member["nationid"])
                                    if discord is not None:
                                        if int(nation['cities']) <= int(cc):
                                            await mil_audit(self, user.id, nation, mmrs[count], ctx, nodm)
                                count = count + 1
            await ctx.send(embed=discord.Embed(description=output, color=0x9013fe))


    @commands.command(brief="Sets this alliances MMR")
    async def setmmr(self, ctx):
        """Sets this alliances MMR!, simply follow the steps to complete the configuration"""
        output = "Please say the total number of MMRs to be set."
        msg = await ctx.send(embed=discord.Embed(description=output, color=0x9013fe))
        def check(m):
            return m.author == ctx.author
        count = await self.bot.wait_for('message', check=check)
        cnt = count.content
        await asyncio.sleep(1)
        await count.delete()
        try:
            if int(cnt) > 10:
                output = "Please keep your MMR count reasonable! At most, 10 MMRs can be configured!"
                msg = await msg.edit(embed=discord.Embed(description=output, color=0x9013fe))
            else:
                try:
                    c = 1
                    co = 0
                    cities = []
                    for _ in range(int(cnt)):
                        if c == int(count.content):
                            cities.append("100")
                        else:
                            output = f"What is the highest city count for your MMR {c}?"
                            await msg.edit(embed=discord.Embed(description=output, color=0x9013fe))
                            c = c + 1
                            response = await self.bot.wait_for('message', check=check)
                            if len(cities) is not 0:
                                if int(response.content) < int(cities[co]):
                                    output = "The current city count can not be lower than the previous!"
                                    await response.delete()
                                    raise Exception
                                else:
                                    co = co + 1
                            cities.append(response.content)
                            await asyncio.sleep(1)
                            await response.delete()
                    output = "The next city count will be defaulted to cover all remaining nations in your alliance!"
                    await msg.edit(embed=discord.Embed(description=output, color=0x9013fe))
                    await asyncio.sleep(5)
                    mmrs = []
                    for i in cities:
                        if i == '100':
                            output = "Last one, what is the MMR that applies to all remaining nations who haven't already fallen into a category."
                        else:
                            output = f"Alright! What is the MMR for nations with a city count of {i} and below."
                        await msg.edit(embed=discord.Embed(description=output, color=0x9013fe))
                        response = await self.bot.wait_for('message', check=check)
                        true = [5, 5, 5, 3]
                        co = 0
                        ck = response.content.split("/")
                        for i in ck:
                            if int(i) > true[co]:
                                output = """
The value you provided for your MMR was incorrect! Please run the command again and
provide a valid MMR using the format 0/0/0/0 and a max of 5/5/5/3!"""
                                await response.delete()
                                raise Exception
                            else:
                                co = co + 1
                        mmrs.append(response.content)
                        await asyncio.sleep(1)
                        await response.delete()
                    header = """
Alright! We're all done! Go ahead and look over your results! If you see any problems,
just rerun setmmr to redo the configuration process!\n\n"""
                    output = ''
                    co = 0
                    for i in cities:
                        if i == "100":
                            f"{f'For All Other Nations: {mmrs[co]}':<{15}}"
                            output = output + f"{f'For All Other Nations:':<{40}}|{f'{mmrs[co]}':<{10}}\n"
                        else:
                            output = output + f"{f'For Nations with {i} Cities and Below:':<{40}}|{f'{mmrs[co]}':<{10}}\n"
                        co = co + 1
                    mmrs = " ".join(mmrs)
                    cities = " ".join(cities)
                    async with self.bot.pool.acquire() as conn:
                        await conn.execute(
                        "UPDATE guilds SET mmrvalues = ($1), mmrcities = ($2) WHERE guild = ($3)",
                        mmrs,
                        cities,
                        ctx.guild.id,
                    )
                    await msg.edit(embed=discord.Embed(description=header + f"```{output}```", color=0x9013fe))
                except:
                    await msg.edit(embed=discord.Embed(description=output, color=0x9013fe))
        except:
            output = "That wasn't a valid number... Please run the command again and provide a number ranging from 1 through 10!"
            await msg.edit(embed=discord.Embed(description=output, color=0x9013fe))

    @commands.command(brief="Registers a nation with a discord user")
    async def register(self, ctx, id: int, *, dis: discord.Member):
        """Registers a nation with a discord user! To modify a registerd user, use '(prefix)rereg'!"""
        if await role_check(self, ctx) == True:
            async with self.bot.pool.acquire() as conn:
                data = await nation_api(self, id)
                if data["success"] is not False:
                    try:
                        check = await conn.fetchval(
                            "SELECT nation FROM disreg WHERE nation = ($1)", id
                        )
                        if check is not None:
                            if check == id:
                                output = "That nation is already properly registered! No further action is necessary!"
                                col = 0xe40000
                                raise Exception
                            else:
                                output = "There is already a user registered with that nation! If you must register again, please use r!rereg!"
                        else:
                            raise Exception
                        col = 0xe40000
                    except:
                        try:
                            check = await conn.fetchval(
                                "SELECT discordid FROM disreg WHERE discordid = ($1)",
                                dis.id,
                            )
                            if check is not None:
                                if int(check) == int(dis.id):
                                    pass
                                else:
                                    output = "That account is already registered with a nation! If you must register again, please use r!rerege!"
                                    col = 0xe40000
                            else:
                                raise Exception
                        except:
                            await conn.execute(
                                "INSERT INTO disreg VALUES(($1), ($2))", dis.id, id
                            )
                            output = "That nation is now registered!"
                            col = 0xc71a
                else:
                    output = "No such nation exists! Please provide a valid nation id!"
                    col = 0xe40000
                await ctx.send(
                    embed=discord.Embed(
                        title="Discord Registry", description=output, color=col
                    ).set_footer(text="Info Provided By http://politicsandwar.com/api/")
                )

    @commands.command(aliases=["ni"], brief="Provides info about a specified nation")
    async def nation(self, ctx, *, target):
        """Provides information about the nation linked to the ID you have given"""
        try:
            try:
                users = [user.name.lower() for user in ctx.guild.members]
                if len(ctx.message.mentions) == 1:
                    for user in ctx.message.mentions:
                        if user in ctx.guild.members:
                            id = await grab_discord(self, ctx, user.id)
                            user = user.id
                            user = self.bot.get_user(user)
                elif target.lower() in users:
                    for user in ctx.guild.members:
                        if user.name.lower() == target.lower():
                            user = user.id
                            user = self.bot.get_user(user)
                            id = await grab_discord(self, ctx, user)
                            break
                else:
                    user = await grab_discord(self, ctx, target)
                    id = int(id)
            except:
                id = int(target)
            try:
                user = user.mention
            except:
                user = "No Discord Found"
            data = await nation_api(self, id)
            ids = " "
            for I in data["cityids"]:
                ids = ids + f"{I}, "
            embed = discord.Embed(
                title=f"{data['name']} - {data['leadername']}",
                description=f"Discord: {user}",
                color=0x9013fe,
                url=f"https://politicsandwar.com/nation/id={data['nationid']}",
            )
            embed.add_field(name="Score:", value=data["score"])
            embed.add_field(name="Alliance", value=data["alliance"])
            embed.add_field(name="Domestic Policy:", value=data["domestic_policy"])
            embed.add_field(name="War Policy:", value=data["war_policy"])
            embed.add_field(name="Cities:", value=data["cities"], inline=False)
            embed.add_field(name="City IDs:", value=ids[:-2], inline=False)
            embed.add_field(name="Soldiers:", value=data["soldiers"])
            embed.add_field(name="Tanks:", value=data["tanks"])
            embed.add_field(name="Aircraft:", value=data["aircraft"])
            embed.add_field(name="Ships:", value=data["ships"])
            embed.add_field(name="Missiles:", value=data["missiles"])
            embed.add_field(name="Nukes:", value=data["nukes"])
            embed.set_image(url=data["flagurl"])
            embed.set_footer(text="Info Provided By http://politicsandwar.com/api/")
        except:
            embed = discord.Embed(
                description="No such nation exists! Please enter a valid nation id!",
                color=0xe40000,
            )
        await ctx.send(embed=embed)

    @commands.command(
        aliases=["ci"], brief="Provides info about the alliance specified"
    )
    async def cityinfo(self, ctx, *, id):
        """Provides information about the alliance linked to the ID you have given"""
        data = await city_api(self, id)
        nation = await nation_api(self, data["nationid"])
        embed = discord.Embed(
            name="City Info",
            color=0x9013fe,
            description=f"""[{data['nation']} - {data['leader']}](https://politicsandwar.com/nation/id={data['nationid']})""",
        )
        embed.add_field(
            name=f"{data['name']} - General Info",
            inline=False,
            value=f"""```
Infra                  {data['infrastructure']}
Land                   {data['land']}
Crime                  {data['crime']}
Disease                {data['disease']}
Pollution              {data['pollution']}
Commerce               {data['commerce']}```""",
        )
        embed.add_field(
            name="Improvements - Power",
            value=f"""```
Coal Power Plants      {data['imp_coalpower']}
Oil Power Plants       {data['imp_oilpower']}
Nuclear Power Plants   {data['imp_nuclearpower']}
Wind Power Plants      {data['imp_windpower']}```""",
        )
        if nation["continent"] == "Europe":
            resources = f"""
Coal Mines             {data['imp_coalmine']}
Iron Mines             {data['imp_ironmine']}
Lead Mines             {data['imp_leadmine']}
Farms                  {data['imp_farm']}"""
        elif nation["continent"] == "Asia":
            resources = f"""
Oil Wells              {data['imp_oilwell']}
Iron Mines             {data['imp_ironmine']}
Uranium Mines          {data['imp_uramine']}
Farms                  {data['imp_farm']}"""
        elif nation["continent"] == "Africa":
            resources = f"""
Oil Wells              {data['imp_oilwell']}
Bauxite Mines          {data['imp_bauxitemine']}
Uranium Mines          {data['imp_uramine']}
Farms                  {data['imp_farm']}"""
        elif nation["continent"] == "South America":
            resources = f"""
Oil Wells              {data['imp_oilwell']}
Bauxite Mines          {data['imp_bauxitemine']}
Lead Mines             {data['imp_leadmine']}
Farms                  {data['imp_farm']}"""
        elif nation["continent"] == "North America":
            resources = f"""
Coal Mines             {data['imp_coalmine']}
Iron Mines             {data['imp_ironmine']}
Uranium Mines          {data['imp_uramine']}
Farms                  {data['imp_farm']}"""
        elif nation["continent"] == "Australia":
            resources = f"""
Coal Mines             {data['imp_coalmine']}
Bauxite Mines          {data['imp_bauxitemine']}
Lead Mines             {data['imp_leadmine']}
Farms                  {data['imp_farm']}"""
        embed.add_field(name="Improvements - Resources", value=f"```{resources}```")
        embed.add_field(
            name="Improvements - Production",
            value=f"""```
Gas Refinerys          {data['imp_gasrefinery']}
Steel Mills            {data['imp_steelmill']}
Aluminum Refinerys     {data['imp_aluminumrefinery']}
Munitions Factorys     {data['imp_munitionsfactory']}```""",
        )
        embed.add_field(
            name="Improvements - Civil",
            value=f"""```
Police Stations        {data['imp_policestation']}
Hospitals              {data['imp_hospital']}
Recycling Centers      {data['imp_recyclingcenter']}
Subways                {data['imp_subway']}```""",
        )
        embed.add_field(
            name="Improvements - Commerce",
            value=f"""```
Supermarkets           {data['imp_supermarket']}
Banks                  {data['imp_bank']}
Malls                  {data['imp_mall']}
Stadiums               {data['imp_stadium']}```""",
        )
        embed.add_field(
            name="Improvements - Military",
            value=f"""```
Barracks               {data['imp_barracks']}
Factories              {data['imp_factory']}
Hangars                {data['imp_hangar']}
Drydocks               {data['imp_drydock']}```""",
        )
        embed.set_footer(text="Info Provided By http://politicsandwar.com/api/")
        await ctx.send(embed=embed)

    @commands.command(brief="Lists members of the specified alliance")
    async def members(self, ctx, *, id=None):
        """Lists members of the specified alliance. Leave blank to get info about the current alliance."""
        member_list = []
        tc = 0
        if id == None:
            if await role_check(self, ctx) == True:
                async with ctx.typing():
                    data = await alliance_members(self, ctx)
                    member_list.append(" ")
                    i = 0
                    index = len(member_list[0])
                    for nation in data["nations"]:
                        member = await grab_discord(self, ctx, str(nation["nationid"]))
                        try:
                            member = member.mention
                        except:
                            member = "No Discord Found"
                        output_string = (
                            member_list[i] + f"""
Nation: {nation['nation']}  Leader: {nation['leader']}
Cities: {nation['cities']}  City/Project Timer: {nation['cityprojecttimerturns']}
Nation Link: https://politicsandwar.com/nation/id={nation['nationid']}
Discord: {member}\n\n"""
                        )
                        index = len(output_string)
                        if (index + 1) > 2000:
                            i += 1
                            output_string = f"Nation: {nation['nation']}  Leader: {nation['leader']}\nCities: {nation['cities']}  City Project Timer: {nation['cityprojecttimerturns']}\nNation Link: https://politicsandwar.com/nation/id={nation['nationid']}\nDiscord: {member}\n\n"
                            member_list.append(output_string)
                        else:
                            member_list[i] = output_string
            else:
                tc = 1
                await ctx.send(
                    "You lack the required permissions to view this alliances member information! You can still use this command to get a member list by providing the alliance ID."
                )
        else:
            async with ctx.typing():
                data = await nations_api(self)
                member_list.append(" ")
                i = 0
                index = len(member_list[0])
                for nation in data["nations"]:
                    member = await grab_discord(self, ctx, str(nation["nationid"]))
                    try:
                        member = member.mention
                    except:
                        member = "No Discord Found"
                    if id.lower() == (nation["alliance"].lower()):
                        output_string = (
                            member_list[i]
                            + f"Nation: {nation['nation']} Leader: {nation['leader']}\nNation Link: https://politicsandwar.com/nation/id={nation['nationid']}\nDiscord: {member}\n\n"
                        )
                        index = len(output_string)
                        if (index + 1) > 1990:
                            i += 1
                            output_string = f"Nation: {nation['nation']} Leader: {nation['leader']}\nNation Link: https://politicsandwar.com/nation/id={nation['nationid']}\nDiscord: {member}\n\n"
                            member_list.append(output_string)
                        else:
                            member_list[i] = output_string
                    else:
                        try:
                            if int(id) > 0:
                                if int(id) == (nation["allianceid"]):
                                    output_string = (
                                        member_list[i]
                                        + f"Nation: {nation['nation']} Leader: {nation['leader']}\nNation Link: https://politicsandwar.com/nation/id={nation['nationid']}\nDiscord: {member}\n\n"
                                    )
                                    index = len(output_string)
                                    if (index + 1) > 1990:
                                        i += 1
                                        output_string = f"Nation: {nation['nation']} Leader: {nation['leader']}\nNation Link: https://politicsandwar.com/nation/id={nation['nationid']}\nDiscord: {member}\n\n"
                                        member_list.append(output_string)
                                    else:
                                        member_list[i] = output_string
                        except:
                            pass
        gathered = []
        for member in member_list:
            if len(member) > 5:
                gathered.append(member)
            else:
                tc = 1
                await ctx.send(
                    embed=discord.Embed(
                        description="No such alliance exists! Please provide a valid alliance id or name!",
                        color=0xe40000,
                    ).set_footer(
                        text="Info Provided By http://politicsandwar.com/api/"
                    )
                )
                break
        if tc == 0:
            c = 0
            message = await ctx.send(
                embed=discord.Embed(
                    description=gathered[0],
                    color=0x9013fe
                ).set_footer(
                    text="Info Provided By http://politicsandwar.com/api/"
                )
            )
            await message.add_reaction('⬅')
            await asyncio.sleep(1)
            await message.add_reaction('➡')
            await asyncio.sleep(1)
            for _ in range(120):
                backstep = c
                try:
                    reaction, user = await self.bot.wait_for('reaction_add', timeout=10)
                    if str(reaction.emoji) == '➡':
                        c = c + 1
                        await message.edit(
                            embed=discord.Embed(
                                description=gathered[c],
                                color=0x9013fe
                            ).set_footer(
                                text="Info Provided By http://politicsandwar.com/api/"
                            )
                        )
                    elif str(reaction.emoji) == '⬅':
                        c = c - 1
                        await message.edit(
                            embed=discord.Embed(
                                description=gathered[c],
                                color=0x9013fe
                            ).set_footer(
                                text="Info Provided By http://politicsandwar.com/api/"
                            )
                        )
                    await asyncio.sleep(1)
                    await message.remove_reaction(str(reaction.emoji), user)
                except:
                    c = backstep
                

    @commands.command(brief="Lists info about the member specified")
    async def member(self, ctx, target, sel):
        """Lists member information about the nation you have specified. This nation must be in your alliance!"""
        if await role_check(self, ctx) == True:
            try:
                users = [user.name.lower() for user in ctx.guild.members]
                tar = target.replace("mmr", "").replace("warchest", "")
                try:
                    users = [user.name.lower() for user in ctx.guild.members]
                    if len(ctx.message.mentions) == 1:
                        for user in ctx.message.mentions:
                            if user in ctx.guild.members:
                                id = await grab_discord(self, ctx, user.id)
                                member = user.id
                                member = self.bot.get_user(member)
                    elif tar.lower() in users:
                        for user in ctx.guild.members:
                            if user.name.lower() == tar.lower():
                                member = user.id
                                member = self.bot.get_user(member)
                                id = await grab_discord(self, ctx, user)
                                break
                    else:
                        member = await grab_discord(self, ctx, tar)
                        id = int(id)
                except:
                    id = int(tar)
                try:
                    member = member.mention
                except:
                    member = "No Discord Found"
                nation = await nation_api(self, id)
                alliance = await alliance_members(self, ctx)
                for I in alliance["nations"]:
                    if int(id) == int(I["nationid"]):
                        message = ctx.message.content.lower()
                        mmr = discord.Embed(
                            title=f"{I['nation']} - {I['leader']} | MMR Information",
                            description=f"Discord: {member}",
                            url=f"https://politicsandwar.com/nation/id={I['nationid']}",
                            color=0x9013fe
                        )
                        mmr.add_field(name="Soldiers", value=I["soldiers"])
                        mmr.add_field(name="Tanks", value=I["tanks"])
                        mmr.add_field(name="Aircraft", value=I["aircraft"])
                        mmr.add_field(name="Ships", value=I["ships"])
                        mmr.add_field(name="Missiles", value=I["missiles"])
                        mmr.add_field(name="Nukes", value=I["nukes"])
                        mmr.add_field(name="Spies", value=I["spies"])
                        gen = discord.Embed(
                            title=f"{I['nation']} - {I['leader']} | General Information",
                            description=f"Discord: {member}",
                            url=f"https://politicsandwar.com/nation/id={I['nationid']}",
                            color=0x9013fe
                        )
                        gen.add_field(name="Score", value=nation["score"])
                        gen.add_field(name="Cities", value=nation["cities"])
                        gen.add_field(name="City and Project Timer", value=nation["cityprojecttimerturns"])
                        gen.add_field(name="Projected Income", value="NULL")
                        gen.add_field(name="Vacation Mode Turns", value=I["vacmode"])
                        gen.add_field(name="Domestic Policy", value=nation["domestic_policy"])
                        gen.add_field(name="War Policy", value=nation["war_policy"])
                        war = discord.Embed(
                            title=f"{I['nation']} - {I['leader']} | Warchest Information",
                            description=f"Discord: {member}",
                            url=f"https://politicsandwar.com/nation/id={I['nationid']}",
                            color=0x9013fe
                        )
                        war.add_field(name="Money", value=I["money"])
                        war.add_field(name="Food", value=I["food"])
                        war.add_field(name="Coal", value=I["coal"])
                        war.add_field(name="Oil", value=I["oil"])
                        war.add_field(name="Uranium", value=I["uranium"])
                        war.add_field(name="Bauxite", value=I["bauxite"])
                        war.add_field(name="Iron", value=I["iron"])
                        war.add_field(name="Lead", value=I["lead"])
                        war.add_field(name="Gasoline", value=I["gasoline"])
                        war.add_field(name="Munitions", value=I["munitions"])
                        war.add_field(name="Aluminum", value=I["aluminum"])
                        war.add_field(name="Steel", value=I["steel"])
                if sel.lower() == "mmr":
                    embed = mmr
                elif sel.lower() == "warchest":
                    embed = war
                else:
                    embed = gen
                message = await ctx.send(embed=embed.set_footer(text="Info Provided By http://politicsandwar.com/api/"))
                options = [gen, war, mmr]
                await message.add_reaction("\N{REGIONAL INDICATOR SYMBOL LETTER G}")
                await asyncio.sleep(1)
                await message.add_reaction("\N{REGIONAL INDICATOR SYMBOL LETTER W}")
                await asyncio.sleep(1)
                await message.add_reaction("\N{REGIONAL INDICATOR SYMBOL LETTER M}")
                await asyncio.sleep(1)
                for _ in range(120):
                    try:
                        reaction, user = await self.bot.wait_for('reaction_add', check=lambda r, u: r.message.id == message.id, timeout=10)
                        if str(reaction.emoji) == '\N{REGIONAL INDICATOR SYMBOL LETTER G}':
                            embed = options[0]
                        elif str(reaction.emoji) == '\N{REGIONAL INDICATOR SYMBOL LETTER W}':
                            embed = options[1]
                        elif str(reaction.emoji) == '\N{REGIONAL INDICATOR SYMBOL LETTER M}':
                            embed = options[2]
                        await message.remove_reaction(str(reaction.emoji), user)
                        await message.edit(embed=embed.set_footer(text="Info Provided By http://politicsandwar.com/api/"))
                    except Exception as e:
                        print(e)

                    await asyncio.sleep(1)
            except Exception as e:
                await raise_error(self.bot, e, "pw")
                embed = discord.Embed(
                    description="No member found! Either that member is not in this alliance or they are not a registered user!",
                    color=0x9013fe,
                )
                await ctx.send(embed=embed)

    @commands.command(brief="Provides the bank contents")
    async def bank(self, ctx):
        """Provides the bank contents of the current alliance"""
        if await role_check(self, ctx) == True:
            data = await bank_api(self, ctx)
            id = await stored_id(self, ctx)
            async with ctx.typing():
                for bank in data["alliance_bank_contents"]:
                    embed = discord.Embed(
                        title=f"{bank['name']} Bank Contents",
                        color=0x9013fe,
                        url=f"https://politicsandwar.com/alliance/id={id}&display=bank",
                    )
                    embed.add_field(
                        name="Money:",
                        value="$"
                        + str("{:,}".format(float("{0:.2f}".format(bank["money"])))),
                    )
                    embed.add_field(name="Food", value=bank["food"])
                    embed.add_field(name="Coal", value=bank["coal"])
                    embed.add_field(name="Oil", value=bank["oil"])
                    embed.add_field(name="Uranium", value=bank["uranium"])
                    embed.add_field(name="Iron", value=bank["iron"])
                    embed.add_field(name="Bauxite", value=bank["bauxite"])
                    embed.add_field(name="Lead", value=bank["lead"])
                    embed.add_field(name="Gasoline", value=bank["gasoline"])
                    embed.add_field(name="Munitions", value=bank["munitions"])
                    embed.add_field(name="Steel", value=bank["steel"])
                    embed.add_field(name="Aluminum", value=bank["aluminum"])
                    embed.set_footer(
                        text="Info Provided By http://politicsandwar.com/api/"
                    )
            await ctx.send(embed=embed)

    @commands.command(aliases=["laa"], brief="Lists the top 50 alliances.")
    async def listaa(self, ctx, opt=None):
        """Lists the top 50 alliances in order by rank and their ID"""
        data = await alliances_api(self)
        output = ""
        cc = 0
        with open("data/warstat.json") as warchest:
            per = json.load(warchest)
        for alliance in (data["alliances"])[0:50]:
            arank = alliance["rank"]
            aname = alliance["name"]
            aid = alliance["id"]
            if len(aname) > 20:
                aname = aname[0:20] + "..."
            if opt is None:
                output = f'{output}\n{f"{arank}":<{5}}{f"{aname}":<{25}}{f"{aid}":>{5}}'
            elif opt == "mil":
                output = f'{output}\n{f"{aname}":<{25}}{per[cc]} Percent'
            cc = cc + 1
        if opt == "mil":
            await ctx.send(
                embed=discord.Embed(
                    title="Top 50 Alliances Militarization Percentage",
                    description="```" + output + "```",
                    color=0x9013fe,
                ).set_footer(text="Info Provided By http://politicsandwar.com/api/")
            )
        else:
            await ctx.send(
                embed=discord.Embed(
                    title="Top 50 Alliances",
                    description="```" + output + "```",
                    color=0x9013fe,
                ).set_footer(text="Info Provided By http://politicsandwar.com/api/")
            )

    @commands.command(aliases=["aai"], brief="Provides info about the aa specified")
    async def alliance(self, ctx, *, name):
        """Provides information about the alliance you have specified"""
        key = False
        data = await alliances_api(self)
        for I in data["alliances"]:
            if name.lower() == I["name"].lower():
                 key = True
                 id = I["id"]
        if key == True:
            pass
        else:
            id = name
        data = await alliance_api(self, id)
        try:
            if data["irc"] == "":
                dis = "No Discord/IRC Listed!"
            else:
                dis = data["irc"]
            if data["forumurl"] == "":
                forums = "No Forum Link Listed!"
            else:
                forums = data["forumurl"]
            embed = discord.Embed(
                title=f'{data["name"]} - {data["allianceid"]}',
                url=f"https://politicsandwar.com/alliance/id={id}",
                color=0x9013fe,
            )
            embed.add_field(name="Discord/IRC:", value=dis, inline=False)
            embed.add_field(name="Forum Link:", value=forums, inline=False)
            embed.add_field(name="Score:", value=data["score"])
            embed.add_field(name="Members:", value=data["members"])
            embed.add_field(name="Cities:", value=data["cities"])
            embed.add_field(name="Soldiers:", value=data["soldiers"])
            embed.add_field(name="Tanks:", value=data["tanks"])
            embed.add_field(name="Aircraft:", value=data["aircraft"])
            embed.add_field(name="Ships:", value=data["ships"])
            embed.add_field(name="Missiles:", value=data["missiles"])
            embed.add_field(name="Nukes:", value=data["nukes"])
            embed.add_field(name="Treasures", value=data["treasures"])
            embed.set_image(url=data["flagurl"])
            embed.set_footer(text="Info Provided By http://politicsandwar.com/api/")
            await ctx.send(embed=embed)
        except:
            await ctx.send(
                embed=discord.Embed(
                    description="No such alliance exists! Please provide a valid alliance name or id! If providing a name, please make sure that it is case sensative!",
                    color=0xe40000,
                ).set_footer(text="Info Provided By http://politicsandwar.com/api/")
            )

    @commands.command(aliases=["ltp"], brief="Lists the current trade prices")
    async def listtrade(self, ctx):
        """Lists the current trade prices for all materials"""
        data = rsshandle("tradeprices.json")
        embed = discord.Embed(title="Current Trade Prices", color=0x9013fe)
        for I in data:
            embed.add_field(
                name=I["resource"].title(),
                value="Average Price:{0[avgprice]}\nLowest Sell PPU:             {1[price]}\nHighest Buy PPU:           {2[price]}\n".format(
                I, I["highestbuy"], I["lowestbuy"]
                ),
                inline=False,
            )
        await ctx.send(
            embed=embed.set_footer(
                text="Info Provided By http://politicsandwar.com/api/"
            )
        )

    @commands.command(brief="An infrastructure cost calculator")
    async def infra(self, ctx, input: float, tobuy: float, var1=None, var2=None):
        """Provides the cost of infra accurate to +/- $100,000. Provide urban and/or cep as a command variable to trigger urbanization and cce infra discounts.
        Usage: '{prefix(self, ctx)}infra <starting-infra> <amount-to-buy>'"""
        if input < 10000:
            if tobuy < 10000:
                if tobuy > 100:
                    count = 0
                    factor = 0
                    cost = 0
                    r = tobuy / 100 + 1.0
                    for _ in range(int(r)):
                        factor = input + count
                        count = count + 100
                        if tobuy >= 100:
                            buying = 100
                            tobuy = tobuy - 100
                        else:
                            buying = tobuy
                        x = (((factor - 10) ** 2.2) / 710) + 300
                        cost = cost + x * buying
                else:
                    x = (((input - 10) ** 2.2) / 710) + 300
                    cost = x * tobuy
                vars = ["urban", "cce"]
                if var1:
                    var1 = var1.lower()
                    if var1 in vars:
                        cost = cost - cost * .05
                if var2:
                    var2 = var2.lower()
                    if var2 in vars:
                        cost = cost - cost * .05
                embed = discord.Embed(
                    title="Infra Cost Calculator",
                    description="To accomidate for discrepincies in this calculator, please ensure you are capable of paying +/- $100,000 what is given here!",
                    color=0x9013fe,
                )
                embed.add_field(name="Total:", value=f"${cost:,.2f}")
                embed.set_footer(
                    text="Results generated based on equations provided by http://politicsandwar.wikia.com/wiki/"
                )
                await ctx.send(embed=embed)

    @commands.command(brief="A land cost calculator")
    async def land(self, ctx, input: float, tobuy: float):
        """Provides the cost of land accurate to +/- $100,000.
        Usage: '{prefix(self, ctx)}land <starting> <amount-to-buy>'"""
        if input < 10000:
            if tobuy < 10000:
                if tobuy > 500:
                    count = 0
                    factor = 0
                    cost = 0
                    r = tobuy // 500 + 1.0
                    for _ in range(int(r)):
                        factor = input + count
                        count = count + 500
                        if tobuy >= 500:
                            buying = 500
                            tobuy = tobuy - 500
                        else:
                            buying = tobuy
                        x = 0.002 * (factor - 20) ** 2 + 50
                        cost = cost + x * buying
                else:
                    x = 0.002 * (input - 20) ** 2 + 50
                    cost = x * tobuy
                embed = discord.Embed(
                    title="Land Cost Calculator",
                    description="To accomidate for discrepincies in this calculator, please ensure you are capable of paying +/- $100,000 what is given here!",
                    color=0x9013fe,
                )
                embed.add_field(name="Total:", value=f"${cost:,.2f}")
                embed.set_footer(
                    text="Results generated based on equations provided by http://politicsandwar.wikia.com/wiki/"
                )
                await ctx.send(embed=embed)

    @commands.command(aliases=["cc"], brief="A city cost calculator")
    async def citycost(self, ctx, city: int):
        """Provides the cost of the next city accurate to +/- $100,000.
        Usage: '{prefix(self, ctx)}citycost <city-to-buy>'"""
        if city < 100:
            cost = 50000 * (city - 1) ** 3 + 150000 * city + 75000
            embed = discord.Embed(
                title="City Cost Calculator",
                description="To accomidate for discrepincies in this calculator, please ensure you are capable of paying +/- $100,000 what is given here!",
                color=0x9013fe,
            )
            embed.add_field(name="Total:", value=f"${cost:,.2f}")
            embed.set_footer(
                text="Results generated based on equations provided by http://politicsandwar.wikia.com/wiki/"
            )
            await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(politics_and_war(bot))
