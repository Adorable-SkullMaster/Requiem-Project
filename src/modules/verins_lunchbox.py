
import json
import os
import random
import discord
from bs4 import BeautifulSoup
from discord.ext import commands
from imgurpython import ImgurClient
from core.resourcehandler import rsshandle
import asyncio


class verins_lunchbox:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def cuddle(self, ctx, target: discord.Member=None):
        dict = rsshandle("urldictionary.json")
        cuddles = dict["cuddles"]
        def check(reaction, user):
            return user == target
        if target is None:
            msg = await ctx.send(embed=discord.Embed(description="Wha??? BAKA! Why would you want to cuddle with me???"))
            await asyncio.sleep(5)
            embed = discord.Embed(description="fine... if you say so... baka!")
            embed.set_image(url='https://media.giphy.com/media/3rgXBEDrK1dtiZzN4c/giphy.gif')
            await msg.edit(embed=embed)
        elif target == ctx.author:
            embed=discord.Embed(
                description="You can't cuddle with yourself silly"
            )
            await ctx.send(embed=embed)
        else:
            msg = await ctx.send(embed=discord.Embed(
                description=f"Hey {target.mention}, {ctx.author.mention} wants to cuddle! Do you accept?"
                ))
            await msg.add_reaction('✅')
            await msg.add_reaction('❎')
            try:
                reaction, user = await self.bot.wait_for('reaction_add', check=check, timeout=60)
                if str(reaction.emoji)== '✅':
                    embed=discord.Embed(
                        description=f"{target.mention} has accepted your cuddle request"
                    )
                    embed.set_image(url=random.choice(cuddles["accepted"]))
                elif str(reaction.emoji) == '❎':
                    embed=discord.Embed(
                        description=f"Your request for cuddles has been rejected {ctx.author.mention}!"
                    )
                    embed.set_image(url=random.choice(cuddles["rejected"]))
            except:
                embed=discord.Embed(
                    description=f"Didn't even care enough to reject you {ctx.author.mention}..."
                )
                embed.set_image(url=random.choice(cuddles["ignored"]))
            await msg.clear_reactions()
            await msg.edit(embed=embed)

    @commands.command()
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def lewd(self, ctx):
        resp = rsshandle("urldictionary.json")
        embed=discord.Embed(color=0x9013fe)
        embed.set_image(url=random.choice(resp["lewd"]))
        await ctx.send(embed=embed)

    #@commands.cooldown(1, 2.5, type=commands.BucketType.user)
    #@commands.command(brief="A meme from my finest collection")
    async def pwmeme(self, ctx):
        """My finest collection of PW related memes! Made in Murica"""
        img_list = os.listdir("data/images/pw_memes")
        img = random.choice(img_list)
        await ctx.send(file=discord.File("data/images/pw_memes/" + img))

    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    @commands.command(brief="Provides a random number fact")
    async def numberfact(self, ctx, *, id="random"):
        """Provides a fact about the number you have provided. Attach /year, /math, /trivia, or /date
        to the end of your number to specify which kind of fact you wish to receive"""
        url = f"http://numbersapi.com/{id}"
        async with self.bot.csess.get(url) as r:
            await ctx.send(
                embed=discord.Embed(
                    description=(await r.read()).decode("utf-8"), color=0xc71a
                ).set_footer(text="Results Provided By https://numbersapi.com/")
            )

    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    @commands.command(brief="Provides a video based on selected genre")
    async def gimme(self, ctx, *, item=None):
        """Provides a link to a random youtube video based on the genre you have specified"""
        data = rsshandle("urldictionary.json")
        data = data["gimme"]
        sourcelist = "\n".join(data)
        if item is None:
            embed = discord.Embed(
                description=f"```Please specify a valid genre! Videos can be provided for the following:\n{sourcelist}```",
                colour=discord.Colour(0xe40000),
            )
            await ctx.send(embed=embed)
        else:
            try:
                output = random.choice(data[f"{item.lower()}"])
                await ctx.send(f"<{output}>")
            except:
                embed = discord.Embed(
                    description=f"```That was not a valid genre! Videos can be provided for the following:\n{sourcelist}```",
                    colour=discord.Colour(0xe40000),
                )
                await ctx.send(embed=embed)

    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    @commands.command(brief="Provides a random cat fact")
    async def catfact(self, ctx):
        """Provides a random cat factProvides a random cat fact."""
        async with self.bot.csess.get("https://catfact.ninja/fact") as r:
            data = await r.json(content_type="application/json")
        await ctx.send(
            embed=discord.Embed(description=data["fact"], color=0x9013fe).set_footer(
                text="Results Provided By https://catfact.ninja/facts"
            )
        )

    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    @commands.command(brief="Sends a random doggo")
    async def randomdoggo(self, ctx):
        """Provides a random doggo image. Prepare for ultimate adorableness."""
        async with self.bot.csess.get("https://dog.ceo/api/breeds/image/random") as r:
            url = (await r.json())["message"]
        await ctx.send(
            embed=discord.Embed(color=0xc71a)
            .set_image(url=url)
            .set_footer(text="Results Provided By https://dog.ceo/api/")
        )

    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    @commands.command(brief="Provides some random sebisauce")
    async def sebisauce(self, ctx):
        """Provides some random sebi from the sebisauce github repository"""
        hrefs = []
        sebisauce = []
        url = "https://github.com/AnakiKaiver297/sebisauce"
        async with ctx.typing():
            async with self.bot.csess.get(url) as response:
                html = await response.read()
            bs = BeautifulSoup(html, "lxml")
            for link in bs.find_all("a"):
                if link.has_attr("href"):
                    hrefs.append(link.attrs["href"])
            for i in hrefs:
                if "sebisauce/blob/master" in str(i):
                    sebisauce.append(i)
        im = "https://github.com" + random.choice(sebisauce) + "?raw=true"
        embed = discord.Embed(title="\t", description="\t", color=0x9013fe)
        embed.set_image(url=im)
        embed.set_footer(
            text="Data © Sebi's Bot Tutorial contributors, discord.gg/GWdhBSp"
        )
        await ctx.send(embed=embed)

    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    @commands.command(name="8ball", brief="Predicts your future.")
    async def fortune(self, ctx, *, content=None):
        """Predicts your future? Provides answers to your deepest darkest questions? Who knows, ask it and find out"""
        response = [
            "Impossible",
            "Highly Unlikely",
            "Your chances are fair",
            "I'd say your odds are decent",
            "It is inevitable",
        ]
        if content == None:
            output = "Is your life so completely devoid of meaning that you haven't even the knowledge of what you were seeking to ask me?"
        else:
            if len(content) <= 7:
                output = "Are you going to ask me a real question, or are you just going to continue wasting my time?"
            else:
                if content.endswith("?"):
                    output = random.choice(response)
                else:
                    output = "Is that a question? I couldn't tell..."
        await ctx.send(embed=discord.Embed(description=output, colour=0x9013fe))

    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    @commands.command(brief="Sends a random dad joke")
    async def dadjoke(self, ctx):
        """Sends a random dad joke. May or may not give you cancer"""
        async with self.bot.csess.get(
            "https://icanhazdadjoke.com/", headers={"Accept": "application/json"}
        ) as r:
            data = await r.json(content_type="application/json")
        embed = discord.Embed(colour=0x9013fe, description=data["joke"])
        embed.set_footer(text="Jokes provided by https://icanhazdadjoke.com/")
        await ctx.send(embed=embed)

    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    @commands.command(brief="Search imgur for a gif or image")
    async def imgur(self, ctx, *, search=None):
        """Search Imgur for an image or gif."""
        client_id = "d8842287e8ca723"
        client_secret = "cb2978d0cc412c01326e5f64d6bd81e2a2d2c165"
        client = ImgurClient(client_id, client_secret)
        if search == None:
            items = client.gallery_random()
        else:
            items = client.gallery_search(search)
        list = []
        for I in items:
            list.append(I.link)
        await ctx.send(
            embed=discord.Embed(color=0x9013fe).set_image(url=random.choice(list))
        )

    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    @commands.command(brief="Rock, Paper, Scissors. Shall we play?")
    async def rps(self, ctx, choice):
        """Rock, Paper, Scissors! Shall we play?"""
        choice = choice.lower()
        choices = ["rock", "paper", "scissors"]
        rps = random.choice(["Rock", "Paper", "Scissors"])
        if choice in choices:
            embed = discord.Embed(
                title="Rock, Paper, Scissors!",
                description=f"I choose {rps}",
                color=0x9013fe,
            )
            if choice == "paper":
                if rps == "Rock":
                    embed.add_field(name="You Won!", value="Nice!")
                if rps == "Scissors":
                    embed.add_field(name="I Won!", value="Better luck next time!")
                if rps == "Paper":
                    embed.add_field(name="Its a Draw!", value="Go for another round?")
            elif choice == "rock":
                if rps == "Rock":
                    embed.add_field(name="Its a Draw!", value="Go for another round?")
                if rps == "Scissors":
                    embed.add_field(name="You Won!", value="Nice!")
                if rps == "Paper":
                    embed.add_field(name="I Won!", value="Better luck next time!")
            elif choice == "scissors":
                if rps == "Rock":
                    embed.add_field(name="I Won!", value="Better luck next time!")
                if rps == "Scissors":
                    embed.add_field(name="Its a Draw!", value="Go for another round?")
                if rps == "Paper":
                    embed.add_field(name="You Won!", value="Nice!")
        else:
            embed = discord.Embed(
                title="Rock, Paper, Scissors!",
                description="I'm not sure thats how you play rock paper scissors :eyes:",
                colour=0x9013fe,
            )
        await ctx.send(embed=embed)

    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    @commands.command(brief="Instructions on using google")
    async def search(self, ctx, *, question):
        """Provides instructions on how to use google! Just ask a question and
        it will show you how to find the answer."""
        question = question.replace(" ", "+")
        await ctx.send(embed=discord.Embed(title=f"<http://lmgtfy.com/?q={question}>"))

    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    @commands.command(brief="Chuck Norris jokes, you dig")
    async def chucknorris(self, ctx):
        """Chuck Norris jokes, you dig?"""
        async with self.bot.csess.get("https://api.chucknorris.io/jokes/random") as r:
            data = await r.json(content_type="application/json")
        embed = discord.Embed(description=data["value"], color=0x9013fe)
        embed.set_footer(text="Results Provided By https://api.chucknorris.io/")
        await ctx.send(embed=embed)

    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    @commands.command(brief="Press F to pay respects")
    async def f(self, ctx):
        """Press F to pay respects..."""
        message = await ctx.send(
            embed=discord.Embed(
                title="Press :regional_indicator_f: to pay respects!", color=0x9013fe
            )
        )
        await message.add_reaction("\N{REGIONAL INDICATOR SYMBOL LETTER F}")


def setup(bot):
    bot.add_cog(verins_lunchbox(bot))
