import discord
from core.error_handling import raise_error
import json
from src.globalFunctions import randColour



def credentials():
    with open("../credentials.json") as credentials:
        data = json.load(credentials)
    return data

class event_handler:
    def __init__(self, bot):
        self.bot = bot


    async def on_guild_channel_create(self, channel):
        async with self.bot.pool.acquire() as conn:
            muterole = await conn.fetchval(
                "SELECT muterole FROM guilds WHERE guild = ($1)", channel.guild.id
            )
            if muterole is not 0:
                for role in channel.guild.roles:
                    if role.id is muterole:
                        await channel.set_permissions(role, send_messages=False)
                        break

    async def on_member_join(self, member):
        try:
            async with self.bot.pool.acquire() as conn:
                guild, greetchan, autorole, greetcustom, greetembed = await conn.fetchrow(
                    "SELECT guild, greetchan, autorole, greetmsg, greetembed FROM guilds WHERE guild = ($1)",
                    member.guild.id,
                )
                if member.guild.id == guild:
                    if greetchan is not 0:
                        channel = self.bot.get_channel(greetchan)
                        if greetcustom is "0":
                            msg = f"Welcome to the guild {member.mention}!"
                        else:
                            msg = greetcustom.replace(f"%user%", f"{member.mention}").replace(
                                f"%members%", f"{len(member.guild.members)}"
                            )
                        if greetembed is True:
                            embed = discord.Embed(color=randColour(), description=msg)
                            embed.set_author(
                                name="User Joined!", icon_url=member.guild.icon_url
                            )
                            embed.set_thumbnail(url=member.avatar_url)
                            embed.set_footer(
                                text=f"{member.guild.name} - {channel.name.title()}"
                            )
                            await channel.send(embed=embed)
                        else:
                            await channel.send(msg)
                    if autorole is not 0:
                        try:
                            await member.add_roles(
                                discord.utils.get(member.guild.roles, id=autorole)
                            )
                        except:
                            pass
        except Exception as error:
            title = "an event: 'on_member_join'"
            await raise_error(self, error, title, ctx=None)

    async def on_member_remove(self, member):
        try:
            async with self.bot.pool.acquire() as conn:
                guild, leavechan, logs, leavecustom, leaveembed = await conn.fetchrow(
                    "SELECT guild, leavechan, logs, leavemsg, leaveembed FROM guilds WHERE guild = ($1)",
                    member.guild.id,
                )
                if member.guild.id == guild:
                    if logs is not 0:
                        logchan = self.bot.get_channel(logs)
                        async for entry in member.guild.audit_logs(
                                action=discord.AuditLogAction.kick
                        ):
                            if f"{entry.target}" == f"{member}":
                                embed = discord.Embed(
                                    color=randColour(),
                                    description=f"{entry.target} was kicked by {entry.user}",
                                )
                                embed.set_author(
                                    name="User Kicked!", icon_url=member.guild.icon_url
                                )
                                embed.set_thumbnail(url=member.avatar_url)
                                embed.set_footer(
                                    text=f"{member.guild.name} - {logchan.name.title()}"
                                )
                                await logchan.send(embed=embed)
                                break
                    if leavechan is not 0:
                        channel = self.bot.get_channel(leavechan)
                        if leavecustom is "0":
                            msg = f"{member.mention} left the guild!"
                        else:
                            msg = msg.replace(f"%user%", f"{member.mention}").replace(
                                f"%members%", f"{len(member.guild.members)}"
                            )
                        if leaveembed is True:
                            embed = discord.Embed(color=randColour(), description=msg)
                            embed.set_author(
                                name="User Left!", icon_url=member.guild.icon_url
                            )
                            embed.set_thumbnail(url=member.avatar_url)
                            embed.set_footer(
                                text=f"{member.guild.name} - {channel.name.title()}"
                            )
                            await channel.send(embed=embed)
                        else:
                            await channel.send(msg)
        except Exception as error:
            title = "an event: 'on_member_remove'"
            await raise_error(self, error, title, ctx=None)

    async def on_member_update(self, before, after):
        try:
            async with self.bot.pool.acquire() as conn:
                guild, logs = await conn.fetchrow(
                    "SELECT guild, logs FROM guilds WHERE guild = ($1)", before.guild.id
                )
                if before.guild.id == guild:
                    if logs is not 0:
                        if before.roles is not after.roles:
                            logchan = self.bot.get_channel(logs)
                            async for entry in before.guild.audit_logs(
                                    action=discord.AuditLogAction.member_role_update
                            ):
                                if f"{entry.target}" == f"{before}":
                                    if len(before.roles) > len(after.roles):
                                        ar = [I.name for I in after.roles]
                                        for I in before.roles:
                                            if I.name not in ar:
                                                output = f'''
                                                Role "{I.name}" was removed from {before.name} by {entry.user}!
                                                '''
                                    elif len(after.roles) > len(before.roles):
                                        br = [I.name for I in before.roles]
                                        for I in after.roles:
                                            if I.name not in br:
                                                output = f'Role "{I.name}" was added to {before.name} by {entry.user}!'
                            embed = discord.Embed(
                                title="User Roles Updated!",
                                description=output,
                                color=randColour(),
                            )
                            embed.set_thumbnail(url=after.avatar_url)
                            await logchan.send(embed=embed)
        except Exception as error:
            title = "an event: 'on_message_update'"
            await raise_error(self, error, title, ctx=None)

    async def on_message_edit(self, before, after):
        try:
            if before.author.bot is False:
                await self.bot.process_commands(after)
                async with self.bot.pool.acquire() as conn:
                    guild, logs = await conn.fetchrow(
                        "SELECT guild, logs FROM guilds WHERE guild = ($1)",
                        before.guild.id,
                    )
                    if before.guild.id == guild:
                        if logs is not 0:
                            if len(before.embeds) == 0:
                                embed = discord.Embed(
                                    title="Message Edited",
                                    description=f"Author: {before.author.name}",
                                    color=randColour(),
                                )
                                embed.add_field(
                                    name="Before", value=before.content, inline=False
                                )
                                embed.add_field(
                                    name="After", value=after.content, inline=False
                                )
                                await self.bot.get_channel(logs).send(embed=embed)
            elif after.author.id == self.bot.user.id:
                for embed in after.embeds:
                    if embed.title == "Requiem DEV - UPDATE - HANDOFF" and credentials()["default_prefix"] == "r!":
                        await asyncio.sleep(10)
                        embed = discord.Embed(
                            title="Requiem MAIN - UPDATE - HANDOFF",
                            description="HANDOFF COMPLETE! Requiem MAIN updating....",
                            color=0x9013fe
                        )
                        await after.edit(embed=embed)
                        halt = True
                        await asyncio.sleep(10)
                        os.chdir("..\\")
                        embed=discord.Embed(
                            title="Requiem MAIN -UPDATE - Finalize",
                            description="Downloading updates.... please stand by! I will restart when finished!",
                            color=0000
                        )
                        await after.edit(embed=embed)
                        os.popen("git pull")
                        await asyncio.sleep(5)
                        embed=discord.Embed(
                            title="Requiem MAIN - UPDATE - Complete",
                            description="I have finished gathering the update from the repo! I will now restart!",
                            color=0000
                        )
                        await after.edit(embed=embed)
                        await self.bot.logout()

        except Exception as error:
            title = "an event: 'on_message_update'"
            await raise_error(self, error, title, ctx=None)

    async def on_message_delete(self, message):
        try:
            if message.author.bot is False:
                async with self.bot.pool.acquire() as conn:
                    guild, logs = await conn.fetchrow(
                        "SELECT guild, logs FROM guilds WHERE guild = ($1)",
                        message.guild.id,
                    )
                    if message.guild.id == guild:
                        if logs is not 0:
                            if len(message.embeds) == 0:
                                logchan = self.bot.get_channel(logs)
                                embed = discord.Embed(
                                    title="Message Deleted",
                                    description=f"Author: {message.author.name}",
                                    color=randColour(),
                                )
                                embed.add_field(
                                    name="Message", value=message.content, inline=False
                                )
                                await logchan.send(embed=embed)
        except Exception as error:
            title = "an event: 'on_message_delete'"
            await raise_error(self, error, title, ctx=None)

    async def on_message(self, message):
        try:
            if message.author.bot is False:
                async with self.bot.pool.acquire() as conn:
                    invpro, prefix = await conn.fetchrow(
                        "SELECT invblk, prefix FROM guilds WHERE guild = ($1)",
                        message.guild.id,
                    )
                    if "discord.gg" in message.content:
                        if invpro == True:
                            await message.delete()
                            self.bot.logger.info(
                                f"INVITE PROTECTION SERVICE | Invite Deleted! User: {message.author.name} | Channel: {message.channel.name} | Guild: {message.guild.name}"
                            )
                    if message.guild.me in message.mentions:
                        if prefix != "x!":
                            await message.channel.send(
                                embed=discord.Embed(
                                    title=f'My prefix on this guild is "{prefix}"',
                                    colour=0x9013fe,
                                )
                            )
                    if message.content.startswith("~~"):
                        if message.author.id != credentials()["ownerid"]:
                            embed=discord.Embed(
                                title="DEVELOPMENT BRANCH",
                                description=f"""
{message.author.mention} You are using the Requiem DEV Branch!
It is highly recommended you use the ACTIVE branch! To find my
ACTIVE branch prefix, just ping me!"""
                            )
                            await message.channel.send(embed=embed)
        except Exception as error:
            title = "an event: 'on_message'"
            await raise_error(self, error, title, ctx=None)

    async def on_command_completion(self, ctx):
        self.bot.logger.info(
            f" User: {ctx.author.name} | Command: {ctx.command} | Channel: {ctx.channel.name} | Guild: {ctx.guild.name}"
        )

    async def on_command_error(self, ctx, error):
        try:
            title = f"a command: '{ctx.command.name}'"
            await raise_error(self.bot, error, title, ctx)
        except:
            pass

    async def on_guild_join(self, guild):
        try:
            async with self.bot.pool.acquire() as conn:
                await conn.execute("INSERT INTO guilds VALUES(($1))", guild.id)
                self.bot.logger.info(
                    f"Requiem Joined: {guild.name}! A database entry has been created!"
                )
        except Exception as error:
            title = "an event: 'on_guild_join'"
            await raise_error(self, error, title, ctx=None)

    async def on_guild_remove(self, guild):
        try:
            async with self.bot.pool.acquire() as conn:
                await conn.execute(f"DELETE FROM guilds WHERE guild = ($1)", guild.id)
                self.bot.logger.info(
                    f"Requiem Left: {guild.name}! The database entry for this server has been removed!"
                )
        except Exception as error:
            title = "an event: 'on_guild_remove'"
            await raise_error(self, error, title, ctx=None)


def setup(bot):
    bot.add_cog(event_handler(bot))
