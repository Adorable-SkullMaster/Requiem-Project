
from discord.ext import commands
import discord
import asyncio
import platform
import logging


class moderation:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(brief="Creates a guild invite")
    @commands.has_permissions(create_instant_invite=True)
    async def invite(self, ctx):
        """Used to create a guild invite in the current channel."""
        invite = await ctx.channel.create_invite()
        await ctx.send(invite)

    @commands.command(brief="Kicks the specified user")
    @commands.has_permissions(kick_members=True)
    async def kick(self, ctx, user: discord.User):
        """Kicks the specified user from the guild!"""
        await ctx.guild.kick(user)
        await ctx.send(
            embed=discord.Embed(description=f"{user} has been kicked from the guild!")
        )

    @commands.command(brief="Bans the specified user")
    @commands.has_permissions(ban_members=True)
    async def ban(self, ctx, user: discord.User, *, reason=None):
        """Bans the specified user from the guild!"""
        if reason is not None:
            await user.send(
                embed=discord.Embed(
                    description=f"You were banned from {ctx.guild.name} for the reason:\n{reason}",
                    color=0x9013fe,
                )
            )
        await ctx.guild.ban(user, reason=reason, delete_message_days=1)
        await ctx.send(
            embed=discord.Embed(description="{user} has been banned from the guild!")
        )

    @commands.command(brief="Softbans the specified user")
    @commands.has_permissions(ban_members=True)
    async def softban(self, ctx, user: discord.User, *, reason=None):
        """Softbans the specified user from the guild! Also deletes their messages from the last 24 hours!"""
        if reason is not None:
            await ctx.send(
                embed=discord.Embed(
                    description=f"You were softbanned from the guild {ctx.guild.name} for the reason:\n\n```{reason}!```\n\nYou may rejoin the guild at any time! Repeated offenses may result in a permanent ban!"
                )
            )
        await ctx.guild.ban(user, reason=reason, delete_message_days=1)
        await ctx.guild.unban(user)
        await ctx.send(
            embed=discord.Embed(
                title=f"```{user.name} was softbanned from the guild! Their messages from the last 24 hours have been purged!```",
                color=0x9013fe,
            )
        )

    @commands.group(brief="Channel related commands")
    @commands.has_permissions(manage_channels=True)
    async def channel(self, ctx):
        """Usage is {prefix}channel >command<\nChannel commands are as follows:"""
        pass

    @channel.command(name="create", brief="Creates a channel")
    async def c1(self, ctx, channel):
        """Used to create a channel."""
        if channel in [I.name for I in ctx.guild.channels]:
            output = "That channel already exists!"
        else:
            channel = await ctx.guild.create_text_channel(channel)
            output = f"The channel '{channel}' has been created!"
        await ctx.send(embed=discord.Embed(title=output, color=0x9013fe))

    @channel.command(name="delete", brief="Deletes a channel")
    async def c2(self, ctx, channel: discord.TextChannel):
        """Used to delete a channel!"""
        if channel in [I for I in ctx.guild.channels]:
            channel = await channel.delete()
            output = f"The channel '{channel}' has been deleted!"
        await ctx.send(embed=discord.Embed(title=output, color=0x9013fe))

    @commands.group(brief="Role related commands")
    @commands.has_permissions(manage_roles=True)
    async def role(self, ctx):
        """Usage is {prefix}role >command<\nRole commands are as follows:"""
        pass

    @role.command(brief="Creates a role")
    async def create(self, ctx, *, crole: str):
        """Used to create a role. Role created will have all default permissions"""
        if crole in [I.name for I in ctx.guild.roles]:
            output = "That role already exists!"
        else:
            role = await ctx.guild.create_role(name=crole)
            output = f"{role} created successfully!"
        await ctx.send(embed=discord.Embed(title=output, color=0x9013fe))

    @role.command(brief="Deletes a role")
    async def delete(self, ctx, role: discord.Role):
        """Used to delete a role."""
        if role in ctx.guild.roles:
            await role.delete()
            output = f"{role} deleted successfully!"
        else:
            output = "No such role exists!"
        await ctx.send(embed=discord.Embed(title=output, color=0x9013fe))

    @role.command(brief="Adds a role to a user")
    async def add(self, ctx, username: discord.Member, *, role: discord.Role):
        """Used to add a role to a user."""
        if role in username.roles:
            output = "The user specified already has that role!"
        else:
            await username.add_roles(role)
            output = f"{role} added to {username}!"
        await ctx.send(embed=discord.Embed(title=output, color=0x9013fe))

    @role.command(brief="Removes a role from a user")
    async def remove(self, ctx, modrole, *, username: discord.Member):
        """Used to remove a role from a user."""
        role = discord.utils.get(ctx.guild.roles, name=f"{modrole}")
        if role in username.roles:
            await username.remove_roles(role)
            output = f"{role} removed from {username}!"
        else:
            output = "The user specified does not have that role!"
        await ctx.send(embed=discord.Embed(title=output, color=0xc71a))

    @commands.command(brief="Deletes a specified number of messages")
    @commands.has_permissions(manage_messages=True)
    async def purge(self, ctx, num=None):
        """Purges a specified number of messages from the current channel."""
        await ctx.message.delete()
        username = ctx.message.mentions
        try:
            purge = int(num)
        except (ValueError, TypeError):
            purge = 100
        if len(username) == 0:
            if num == None:
                deleted = await ctx.channel.purge(
                    limit=100, check=lambda m: m.author == ctx.bot.user
                )
            else:
                deleted = await ctx.channel.purge(limit=int(purge))
        else:
            deleted = await ctx.channel.purge(
                limit=int(purge), check=lambda m: m.author == username[0]
            )
        await ctx.send(
            embed=discord.Embed(
                title=f"Deleted {len(deleted)} message(s)", color=0x9013fe
            ),
            delete_after=10,
        )

    @commands.command(brief="Changes a users nickname")
    @commands.has_permissions(manage_nicknames=True)
    async def setnick(self, ctx, userName: discord.Member, *, nick):
        """Changes the nickname of the specified user."""
        await userName.edit(nick=nick)
        await ctx.send(
            embed=discord.Embed(
                title=f"Changed {userName.name}'s nickname to {nick}", color=0x9013fe
            )
        )

    @commands.command(brief="Sets/creates a muterole")
    @commands.has_permissions(manage_roles=True)
    async def setmute(self, ctx, role=None):
        async with self.bot.pool.acquire() as conn:
            prefix, muterole = await conn.fetchrow(
                "SELECT prefix, muterole FROM guilds WHERE guild = ($1)", ctx.guild.id
            )
            check = False
            if muterole is 0:
                for rle in ctx.guild.roles:
                    if rle.name == role:
                        role = rle
                        output = f"I will now use '{role}' when muting users!"
                        check = True
                        break
                if check is False:
                    if role is None:
                        role = await ctx.guild.create_role(name="Mute")
                        output = "A mute role has been created!"
                    else:
                        role = await ctx.guild.create_role(name=role)
                        output = f"A new mute role has been created! I will now use '{role}' when muting users!"
                    for channel in ctx.guild.channels:
                        await channel.set_permissions(role, send_messages=False)
                role = role.id
            else:
                try:
                    for role in ctx.guild.roles:
                        if role.id is muterole:
                            await role.delete()
                            break
                    output = f'Mute role has been deleted! You will need to set the mute role again using "{prefix}setmute [role]"'
                except:
                    pass
                    output = f'I was unable to delete the mute role! The role will need to be deleted manually if you chooes to do so! You will need to set the mute role again using "{prefix}setmute [role]"'
                role = 0
            await conn.execute(
                "UPDATE guilds SET muterole = ($1) WHERE guild = ($2)",
                role,
                ctx.guild.id,
            )
            await ctx.send(embed=discord.Embed(description=output, color=0x9013fe))

    @commands.command(brief="Punishes a user and cleans up the channel")
    @commands.has_permissions(manage_roles=True)
    async def subdue(self, ctx, user: discord.Member):
        """Mutes the specified user and deletes their last 100 messages!"""
        async with self.bot.pool.acquire() as conn:
            prefix, muterole = await conn.fetchrow(
                "SELECT prefix, muterole FROM guilds WHERE guild = ($1)", ctx.guild.id
            )
        await ctx.channel.purge(
            limit=100, check=lambda m: m.author == user
        )
        if muterole is not 0:
            for role in ctx.guild.roles:
                if role.id == muterole:
                    await user.add_roles(role)
            output = f"{user.mention} has been muted! Their last 100 messages have been deleted!"
        else:
            output = f'There is currently no mute role set! Use "{prefix}setmute [role]" to set a mute role! {user.mention} has had their last 100 messages deleted!'
        await ctx.send(embed=discord.Embed(description=output, color=0x9013fe))

    @commands.command(brief="Mutes a specified user")
    @commands.has_permissions(manage_roles=True)
    async def mute(self, ctx, user: discord.Member):
        """Mutes the specified user"""
        async with self.bot.pool.acquire() as conn:
            prefix, muterole = await conn.fetchrow(
                "SELECT prefix, muterole FROM guilds WHERE guild = ($1)", ctx.guild.id
            )
        if muterole is not 0:
            for role in ctx.guild.roles:
                if role.id == muterole:
                    if role not in user.roles:
                        await user.add_roles(role)
                        output = f"{user.mention} has been muted!"
                        break
                    else:
                        output = f'That user is already muted! Use "{prefix}unmute [user]" to unmute the user!'
                        break
        else:
            output = f'There is currently no mute role set! Use "{prefix}setmute [role]" to set a mute role!'
        try:
            await ctx.send(embed=discord.Embed(description=output, color=0x9013fe))
        except:
            pass

    @commands.command(brief="Unmutes a specified user")
    @commands.has_permissions(manage_roles=True)
    async def unmute(self, ctx, user: discord.Member):
        """Unmutes the specified user"""
        async with self.bot.pool.acquire() as conn:
            prefix, muterole = await conn.fetchrow(
                "SELECT prefix, muterole FROM guilds WHERE guild = ($1)", ctx.guild.id
            )
        if muterole is not 0:
            for role in ctx.guild.roles:
                if role.id == muterole:
                    if role in user.roles:
                        await user.remove_roles(role)
                        output = f"{user.mention} has been unmuted!"
                        break
                    else:
                        output = f'That user is not currently muted! Use "{prefix}mute [user]" to mute the user!'
        else:
            output = f'There is currently no mute role set! Use "{prefix}setmute [role]" to set a mute role!'
        try:
            await ctx.send(embed=discord.Embed(description=output, color=0x9013fe))
        except:
            pass

    @commands.command(brief="Causes the bot to leave the guild")
    @commands.has_permissions(administrator=True)
    async def exit(self, ctx):
        """Causes the bot to exit the guild! Must have permission MANAGE_GUILD"""
        await ctx.send(embed=discord.Embed(description="Leaving...", color=0x9013fe))
        await self.bot.leave()


def setup(bot):
    bot.add_cog(moderation(bot))
