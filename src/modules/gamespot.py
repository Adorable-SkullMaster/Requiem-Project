
from discord.ext import commands
import discord
import asyncio
import aiohttp
import json


def credentials():
    with open("credentials.json") as credentials:
        data = json.load(credentials)
    return data


class gamespot:
    def __init__(self, bot):
        self.bot = bot

    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    @commands.command(brief="Provides details about an mc server")
    async def mcserver(self, ctx, ip=None):
        """Provides information about the minecraft server provided! Must provide a valid server ip!"""
        if ip == None:
            pass
        else:
            async with self.bot.csess.get(
                f"https://mcapi.us/server/status?ip={ip}",
                headers={"Content-Type": "application/json"},
            ) as response:
                data = await response.json(content_type="application/json")
            if data["online"] == True:
                status = "Online"
            else:
                status = "Offline"
            embed = discord.Embed(title=data["motd"], color=0x9013fe)
            embed.add_field(name="IP:", value=ip)
            embed.add_field(name="Status:", value=status)
            embed.add_field(
                name="Players Online",
                value="{0[now]} of {0[max]}".format(data["players"]),
            )
            embed.set_footer(text="Information provided by https://mcapi.us/")
            await ctx.send(embed=embed)

    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    @commands.command(aliases=["fns"])
    async def fortnitestats(self, ctx, user, platform=None):
        """Provides stats for a fortnite player. That player must be registered on Fortnite Tracker for them to show up here!"""
        try:
            platform = platform.lower()
            if "playstation" in platform:
                platform = "psn"
            elif "xbox" in platform:
                platform = "xbox one"
            elif platform is None:
                platform = "pc"
            async with self.bot.csess.get(
                f"https://api.fortnitetracker.com/v1/profile/{platform}/{user}",
                headers={"TRN-Api-Key": credentials()["fnt-key"]},
            ) as r:
                data = await r.json(content_type="application/json")
            values = []
            for I in data["lifeTimeStats"]:
                values.append(I["value"])
            stats = data["stats"]
            solo = stats["p2"]
            duo = stats["p10"]
            squads = stats["p9"]
            embed = discord.Embed(
                title=f'Fortnite Stats for {user.title()} on the Platform {data["platformNameLong"]}',
                color=0x9013fe,
            )
            embed.add_field(name="Solo Rank", value=solo["trnRating"]["rank"])
            embed.add_field(name="Duo Rank", value=duo["trnRating"]["rank"])
            embed.add_field(name="Squads Rank", value=squads["trnRating"]["rank"])
            embed.add_field(name="Score", value=values[6])
            embed.add_field(name="Matches Played", value=values[7])
            embed.add_field(name="Wins", value=values[8])
            embed.add_field(name="Win Percentage", value=values[9])
            embed.add_field(name="Kills", value=values[10])
            embed.add_field(name="K/D", value=values[11])
            embed.set_footer(
                text="Powered By FortniteTracker! https://api.fortnitetracker.com/"
            )
            await ctx.send(embed=embed)
        except:
            embed = discord.Embed(
                description='I was not able to find that player! Perhaps try providing a platform like "playstation" or "xbox"',
                color=0x9013fe,
            )
        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(gamespot(bot))
