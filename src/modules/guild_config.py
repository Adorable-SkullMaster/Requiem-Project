
from discord.ext import commands
import discord
import asyncio
import platform
import logging
import asyncpg
import json
from src.modules.pw import alliance_members, alliance_api


def credentials():
    with open("credentials.json") as credentials:
        data = json.load(credentials)
    return data


class guild_config:
    def __init__(self, bot):
        self.bot = bot

    @commands.group(brief="Enables/disables user join messages")
    @commands.has_permissions(manage_guild=True)
    async def greet(self, ctx):
        """Enables/disables the user join message"""
        if ctx.invoked_subcommand is None:
            async with self.bot.pool.acquire() as conn:
                greetchan = await conn.fetchval(
                    "SELECT greetchan FROM guilds WHERE guild = ($1)", ctx.guild.id
                )
                if greetchan is 0:
                    chanid = ctx.channel.id
                    output = "User join messages are now enabled!"
                elif greetchan == ctx.channel.id:
                    chanid = 0
                    output = "User join messages are now disabled!"
                else:
                    chanid = ctx.channel.id
                    output = "User join messages will now be sent to this channel!"
                await conn.execute(
                    "UPDATE guilds SET greetchan = ($1) WHERE guild = ($2)",
                    chanid,
                    ctx.guild.id,
                )
                await ctx.send(embed=discord.Embed(title=output, color=0x9013fe))

    @greet.command(brief="Adjusts the greet message")
    async def set(self, ctx, *, message=None):
        """Adjusts the greet message, leave blank to set to default"""
        async with self.bot.pool.acquire() as conn:
            if message is None:
                message = "0"
                output = "User join message set to default!"
                col = 0xc71a
            elif len(message) <= 500:
                message = message
                output = "User join message updated!"
                col = 0xc71a
            else:
                output = "Greet messages can be no greater than 500 characters!"
                col = 0xe40000
            await conn.execute(
                "UPDATE guilds SET greetmsg = ($1) WHERE guild = ($2)",
                message,
                ctx.guild.id,
            )
            await ctx.send(embed=discord.Embed(title=output, colour=col))

    @greet.command(brief="Enables/disables greet message embeds")
    async def embed(self, ctx):
        """Enables/Disables greet message embedding"""
        async with self.bot.pool.acquire() as conn:
            greetembed = await conn.fetchval(
                "SELECT greetembed FROM guilds WHERE guild = ($1)", ctx.guild.id
            )
            if greetembed is False:
                em = True
                output = "User join messages will now be embedded!"
            else:
                em = False
                output = "User join messages will no longer be embedded!"
            await conn.execute(
                "UPDATE guilds SET greetembed = ($1) WHERE guild = ($2)",
                em,
                ctx.guild.id,
            )
            await ctx.send(embed=discord.Embed(title=output, color=0x9013fe))

    @commands.group(brief="Enables/disables user leave messages")
    @commands.has_permissions(manage_guild=True)
    async def leave(self, ctx):
        """Enables/disables the user leave message"""
        if ctx.invoked_subcommand is None:
            async with self.bot.pool.acquire() as conn:
                leavechan = await conn.fetchval(
                    "SELECT leavechan FROM guilds WHERE guild = ($1)", ctx.guild.id
                )
                if leavechan is 0:
                    chanid = ctx.channel.id
                    output = "User leave messages are now enabled!"
                elif leavechan == ctx.channel.id:
                    chanid = 0
                    output = "User leave messages are now disabled!"
                else:
                    chanid = ctx.channel.id
                    output = "User leave messages will now be sent to this channel!"
                await conn.execute(
                    "UPDATE guilds SET leavechan = ($1) WHERE guild = ($2)",
                    chanid,
                    ctx.guild.id,
                )
                await ctx.send(embed=discord.Embed(title=output, colour=0x9013fe))

    @leave.command(name="set", brief="Adjust the leave message")
    async def leave_set(self, ctx, *, message=None):
        """Adjusts the leave message, leave blank to set to default"""
        async with self.bot.pool.acquire() as conn:
            if message is None:
                message = "0"
                output = "User leave message set to default!"
                col = 0xc71a
            elif len(message) <= 500:
                message = message
                output = "User leave message updated!"
                col = 0xc71a
            else:
                output = "Leave messages can be no greater than 500 characters!"
                col = 0xe40000
            await conn.execute(
                "UPDATE guilds SET leavecustom = ($1) WHERE guild = ($2)",
                message,
                ctx.guild.id,
            )
            await ctx.send(embed=discord.Embed(title=output, color=col))

    @leave.command(name="embed", brief="Enables/disables leave message embeds")
    async def leave_embed(self, ctx):
        """Enables/Disables leave message embedding"""
        async with self.bot.pool.acquire() as conn:
            leaveembed = await conn.fetchval(
                "SELECT greetembed FROM guilds WHERE guild = ($1)", ctx.guild.id
            )
            if leaveembed == False:
                em = True
                output = "User leave messages will now be embedded!"
            else:
                em = False
                output = "User leave messages will no longer be embedded!"
            await conn.execute(
                "UPDATE guilds SET leaveembed = ($1) WHERE guild = ($2)",
                em,
                ctx.guild.id,
            )
            await ctx.send(embed=discord.Embed(title=output, color=0x9013fe))

    @commands.command(brief="Enables/disables guild invite protection")
    @commands.has_permissions(manage_channels=True)
    async def invblock(self, ctx):
        """Enables/Disables guild invite protection. If enabled, bot will delete any and all invites in the channels it can see."""
        async with self.bot.pool.acquire() as conn:
            invpro = await conn.fetchval(
                "SELECT invpro FROM guilds WHERE guild = ($1)", ctx.guild.id
            )
            if invpro == True:
                set = False
                output = "Invite protection is now disabled! I will no longer delete invites sent to this guild!"
            else:
                set = True
                output = "Invite protection is now enabled! I will now delete all invites sent to this guild!"
            await conn.execute(
                "UPDATE guilds SET invpro = ($1) WHERE guild = ($2)", set, ctx.guild.id
            )
            await ctx.send(embed=discord.Embed(title=output, color=0x9013fe))

    @commands.has_permissions(administrator=True)
    @commands.group(brief="Displays the current pw configuration state")
    async def pw(self, ctx):
        """Displays the current pw configuration state of the guild."""
        if ctx.invoked_subcommand is None:
            async with self.bot.pool.acquire() as conn:
                pwkey, pwrole, pwaa = await conn.fetchrow(
                    "SELECT pwkey, pwrole, pwaa FROM guilds WHERE guild = ($1)",
                    ctx.guild.id,
                )
                if pwkey == "0":
                    if pwrole == 0:
                        output = "There is currently no role assigned to the api commands and this guild is not currently configured to work with the pw api! Please use r!pw role to assign a role and r!pw config to configure this guild with an api key!"
                        col = 0xe40000
                    else:
                        output = "this guild is not currently configured to work with the pw api! Please use r!pw config to configure this guild with an api key!"
                        col = 0xe40000
                else:
                    if pwrole == 0:
                        output = "There is currently now role assigned to the api commands! Please use r!pw role to assign a role!"
                        col = 0xe40000
                    else:
                        async with self.bot.csess.get(
                            f"http://politicsandwar.com/api/alliance-members/?allianceid={pwaa}&key={pwkey}",
                            headers={"Content-Type": "application/json"},
                        ) as response:
                            data = await response.json(content_type="application/json")
                            if data["success"] == False:
                                output = "The key associated with this guild is outdated! Please provide a new key using r!pw config! The key provided must belong to a government heir or above in the target alliance!"
                                col = 0xe40000
                            else:
                                output = "This guild is properly configured! "
                                col = 0xc71a
                await ctx.send(embed=discord.Embed(title=output, color=col))

    @pw.command(brief="Grants a specific role pw perms")
    async def role(self, ctx, *, role: discord.Role = None):
        """Grants a specific role the necessary perms to access all private pw related commands! Must have perms: ADMINISTRATOR"""
        async with self.bot.pool.acquire() as conn:
            pwrole = await conn.fetchval(
                "SELECT pwrole FROM guilds WHERE guild = ($1)", ctx.guild.id
            )
            if role == None:
                pwrole = 0
                output = "Successfully cleared pw command perms! You will need to set a new role for pw in order to access private pw related commands!"
                col = 0xbd10e0
            elif pwrole == 0:
                pwrole = role.id
                output = f"{role.name} has been granted pw command access!"
                col = 0xbd10e0
            else:
                pwrole = role.id
                output = f"PW command access updated! {role.name} has been granted pw command access!"
                col = 0xbd10e0
            await conn.execute(
                "UPDATE guilds SET pwrole = ($1) WHERE guild = ($2)",
                pwrole,
                ctx.guild.id,
            )
            await ctx.send(embed=discord.Embed(title=output, color=col))

    @pw.command(aliases=["wm"], brief="Enables/disables warmonitoring service")
    async def warmonitor(self, ctx):
        """Enables/disables the war monitoring service on this guild! Must have perms: ADMINISTRATOR"""
        async with self.bot.pool.acquire() as conn:
            warmonitor, pwaa = await conn.fetchrow(
                "SELECT warmonitor, pwaa FROM guilds WHERE guild = ($1)", ctx.guild.id
            )
            if pwaa is 0:
                output = 'You must first configure your alliance id before you can use this! Please use "{prefix}pw config <alliance-id>" to configure your alliance id!'
            else:
                if warmonitor is 0:
                    warmonitor = ctx.channel.id
                    output = "War monitoring enabled! I will now send a notification to this channel any time a war is declared on or by this alliance!"
                elif warmonitor != ctx.channel.id:
                    warmonitor = ctx.channel.id
                    output = "War monitor channel updated! I will now send a notification to this channel any time a war is declared on or by this alliance!"
                else:
                    warmonitor = 0
                    output = "War monitoring is now disabled! I will no longer send war notifications to this guild!"
            await conn.execute(
                "UPDATE guilds SET warmonitor = ($1) WHERE guild = ($2)",
                warmonitor,
                ctx.guild.id,
            )
            await ctx.send(
                embed=discord.Embed(
                    title="War Monitoring", description=output, color=0x9013fe
                )
            )

    @pw.command(aliases=["am"], brief="Enables/disables applicantmonitor service")
    async def applicantmonitor(self, ctx):
        """Enables/disables the applicant monitor service on this guild! Must have perms: ADMINISTRATOR"""
        async with self.bot.pool.acquire() as conn:
            prefix, apploc, pwaa, pwkey = await conn.fetchrow(
                "SELECT prefix, applicantmonitor, pwaa, pwkey FROM guilds WHERE guild = ($1)",
                ctx.guild.id,
            )
            if pwkey == "0":
                if pwaa == "0":
                    output = f'You can not enable the applicant monitor as you have not provided a valid api key and alliance id! To configure this guild, use "{prefix}pw config"'
                else:
                    output = f'You can not enable the applicant monitor as you have not provided a valid api key! To configure this guild, use "{prefix}pw config"'
            else:
                if apploc == 0:
                    apploc = ctx.channel.id
                    output = "I will now send a notification to this channel any time someone applies to this alliance!"
                elif apploc == ctx.channel.id:
                    apploc = 0
                    output = "Applicant Monitor is now disabled! I will no longer send notifications to this channel when a nation applies to join this alliance!"
                else:
                    apploc = ctx.channel.id
                    output = "Applicant Monitor channel updated! I will now send a notification to this channel any time someone applies to this alliance!"

            await conn.execute(
                "UPDATE guilds SET applicantmonitor = ($1) WHERE guild = ($2)",
                apploc,
                ctx.guild.id,
            )
            await ctx.send(embed=discord.Embed(description=output, color=0x9013fe))

    @pw.command(brief="Configures this guild to work with pw api")
    async def config(self, ctx, id: int = None, key=None):
        """Configures the guild to an api key and alliance ID. This key and id is used to access private pw commands! Must have perms: ADMINISTRATOR"""
        await ctx.message.delete()
        async with self.bot.pool.acquire() as conn:
            aamembers = await alliance_members(self, ctx, key, id)
            aa = await alliance_api(self, id)
            pwkey = await conn.fetchval(
                "SELECT pwkey FROM guilds WHERE guild = ($1)", ctx.guild.id
            )
            if id is None:
                output = "This server is no longer linked to an api key!"
                col = 0xc71a
                id = 0
                key = '0'
            elif key is None:
                try:
                    if aa["success"] == True:
                        output = "This alliance is now registered with an ID! Keep in mind you will have access to basic tools such as war monitoring! If you wish to have full access to all pw commands, you must add an api key!"
                        col = 0xf8e71c   
                        key = '0'
                except:
                    output = "I was not able to find an alliance with that ID! Please provide a valid alliance ID..."
                    key = '0'
                    id = 0
                    col = 0xe40000
            else:
                if aamembers["success"] == False:
                    output = "Either the key or the alliance id you have provided are invalid! Please provide a valid key and id! The key provided must belong to a government heir or above in the target alliance!"
                    col = 0xe40000
                    key = '0'
                    id = 0
                elif pwkey == "0":
                    output = "Successfully registered this server with an api key and alliance id!"
                    col = 0xc71a
                else:
                    output = (
                        "Successfully updated this servers api key and alliance id!"
                    )
                    col = 0xc71a
            await conn.execute(
                "UPDATE guilds SET pwkey = ($1), pwaa = ($2) WHERE guild = ($3)",
                key,
                id,
                ctx.guild.id,
            )
            await ctx.send(embed=discord.Embed(title=output, color=col))

    @commands.command(brief="Enables/disables guild autorole")
    @commands.has_permissions(manage_roles=True)
    async def autorole(self, ctx, role: discord.Role = None):
        """Enables/Disables the guilds autorole feature. If enabled, it will grant a role to anyone who joins the server.
        Must have perms: MANAGE_ROLES"""
        async with self.bot.pool.acquire() as conn:
            autorole = await conn.fetchval(
                "SELECT autorole FROM guilds WHERE guild = ($1)", ctx.guild.id
            )
            if role == None:
                output = "Autorole is now off on this guild!"
                ar = 0
            elif autorole == 0:
                ar = role.id
                output = f"Autorole is now enabled on this server! New users will now be given the {role.name} role!"
            else:
                ar = role.id
                output = f"Autorole has been updated! New users will now be given the {role.name} role!"
            await conn.execute(
                "UPDATE guilds SET autorole = ($1) WHERE guild = ($2)", ar, ctx.guild.id
            )
            await ctx.send(embed=discord.Embed(title=output, color=0x9013fe))

    @commands.command(brief="Sets my prefix on this guild")
    @commands.has_permissions(manage_guild=True)
    async def prefix(self, ctx, prefix=None):
        """Sets the bots prefix on this guild. Leave blank to set to default! Must have perms: MANAGE_GUILD"""
        async with self.bot.pool.acquire() as conn:
            if prefix == None:
                pre = credentials()["default_prefix"]
                msg = "Prefix set to default!"
            elif prefix == credentials()["default_prefix"]:
                pre = credentials()["default_prefix"]
                msg = "Prefix set to default!"
            else:
                pre = prefix
                msg = f"Prefix set to {pre}!"
            await conn.execute(
                "UPDATE guilds SET prefix = ($1) WHERE guild = ($2)", pre, ctx.guild.id
            )
            await ctx.send(embed=discord.Embed(description=msg, color=0x9013fe))

    @commands.command(brief="Enables/disables moderation logging")
    @commands.has_permissions(view_audit_log=True)
    async def logs(self, ctx, log=None):
        """Used to set logging on the guild! WARNING! THIS WILL SPAM WHICHEVER CHANNEL IT IS ENABLED ON!
        Must have perms: VIEW_AUDIT_LOG"""
        async with self.bot.pool.acquire() as conn:
            logs = await conn.fetchval(
                "SELECT logs FROM guilds WHERE guild = ($1)", ctx.guild.id
            )
            if logs is not 0:
                logs = 0
                output = "Moderation logging is now disabled!"
            else:
                logs = ctx.channel.id
                output = "Moderation logging is now active!"
            await conn.execute(
                "UPDATE guilds SET logs = ($1) WHERE guild = ($2)", logs, ctx.guild.id
            )
            await ctx.send(embed=discord.Embed(description=output, color=0x9013fe))


def setup(bot):
    bot.add_cog(guild_config(bot))
