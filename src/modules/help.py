
import discord
from discord.ext import commands
from src.globalFunctions import randColour
from core.resourcehandler import rsshandle

class help_cog:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def help(self, ctx, *, sub=None):
        """Displays this help dialogue."""
        async with self.bot.pool.acquire() as conn:
            pre = await conn.fetchval(
                "SELECT prefix FROM guilds WHERE guild = ($1)", ctx.guild.id
            )
            try:
                if sub is None:
                    helptext = rsshandle("helpdoc.txt")
                    embed = discord.Embed(
                        title="Help",
                        description=helptext.format(pre),
                        colour=randColour(),
                    )
                else:
                    sub = sub.lower().replace(" ", "_").replace("'", "")
                    cog = self.bot.get_cog(sub)
                    if cog is not None:
                        coms = self.bot.get_cog_commands(sub)
                        sub = sub.title().replace("_", " ")
                        embed = discord.Embed(
                            title=sub,
                            description=f"Help for {sub} commands, use {pre}help to see all modules\n"
                                        f"Command Usage: {pre}>command<\n\uFFF0",
                            colour=randColour(),
                        )
                        output = ""
                        for I in coms:
                            com = self.bot.get_command(f"{I}")
                            if com.hidden is False:
                                aliases = ", ".join(I for I in com.aliases)
                                if len(aliases) > 0:
                                    output = (
                                        output + f'{f"{com.name} - {aliases}":<{16}} | {com.brief}\n'
                                    )
                                else:
                                    output = (
                                        output + f'{f"{com.name}":<{16}} | {com.brief}\n'
                                    )
                                try:
                                    children = []
                                    for I in com.walk_commands():
                                        children.append(I)
                                    for I in children:
                                        output = (
                                            output + f'{f"--{I.name}":<{16}} | {I.brief}\n'
                                        )
                                except Exception as e:
                                    pass
                        embed.add_field(name="Commands", value=f"```{output}```")
                    else:
                        com = self.bot.get_command(sub)
                        aliases = ", ".join(I for I in com.aliases)
                        if len(aliases) > 1:
                            title = f"{com.name} - {aliases}"
                        else:
                            title = com.name
                        embed = discord.Embed(
                            title=title,
                            description=com.help,
                            colour=randColour(),
                        )
            except Exception as e:
                embed = discord.Embed(
                    title="Help",
                    description=f"Help Usage: {pre}help <command or module name>",
                    colour=discord.Colour(0xe40000),
                )
                embed.add_field(
                    name="Error",
                    value="No such command or module exists! Please provide a valid command or module name!",
                )
            await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(help_cog(bot))
