from src.modules.pw import (
    role_check,
    nation_api,
    city_api,
    alliance_members,
    grab_discord,
    nations_api,
    bank_api,
    stored_id,
    trade_api,
    wars_api,
    war_api,
    applicants,
    alliances_api,
    alliance_api,
)
from core.error_handling import raise_error
from core.resourcehandler import rsshandle
import discord
import asyncio
import random
import time
from src.globalFunctions import randColour


async def defensive_war(self, info, n1, n2, warmonitor):
    embed = discord.Embed(title="New Defensive War!", color=randColour())
    embed.add_field(
        name="Defending Nation:",
        value=f"[{n1['name']}](https://politicsandwar.com/nation/id={info['defender_id']})",
    )
    embed.add_field(
        name="Attacking Nation:",
        value=f"[{n2['name']}](https://politicsandwar.com/nation/id={info['aggressor_id']})",
    )
    embed.add_field(name="Defender Alliance:", value=n1["alliance"])
    embed.add_field(name="Aggressor Alliance:", value=n2["alliance"])
    embed.add_field(
        name="Reason For Declaration:", value=info["war_reason"], inline=False
    )
    embed.add_field(name="War Type:", value=info["war_type"], inline=False)
    embed.add_field(
        name="Defender Info:",
        value=f"""
Soldiers: {n1['soldiers']}
Tanks:    {n1['tanks']}
Aircraft: {n1['aircraft']}
Ships:    {n1['ships']}
Missiles: {n1['missiles']}
Nukes:    {n1['nukes']}""",
    )
    embed.add_field(
        name="Attacker Info:",
        value=f"""
Soldiers: {n2['soldiers']}
Tanks:    {n2['tanks']}
Aircraft: {n2['aircraft']}
Ships:    {n2['ships']}
Missiles: {n2['missiles']}
Nukes:    {n2['nukes']}""",
    )
    embed.set_footer(text="Info Provided By http://politicsandwar.com/api/")
    await self.bot.get_channel(warmonitor).send(embed=embed)


async def aggressive_war(self, info, n1, n2, warmonitor):
    embed = discord.Embed(title="New Offensive War!", color=randColour())
    embed.add_field(
        name="Attacking Nation:",
        value=f"[{n2['name']}](https://politicsandwar.com/nation/id={info['aggressor_id']})",
    )
    embed.add_field(
        name="Defending Nation:",
        value=f"[{n1['name']}](https://politicsandwar.com/nation/id={info['defender_id']})",
    )
    embed.add_field(name="Aggressor Alliance:", value=n2["alliance"])
    embed.add_field(name="Defender Alliance:", value=n1["alliance"])
    embed.add_field(name="Reason For Declaration:", value=info["war_reason"])
    embed.add_field(name="War Type:", value=info["war_type"])
    embed.add_field(
        name="Attacker Info:",
        value=f"""
Soldiers: {n2['soldiers']}
Tanks:    {n2['tanks']}
Aircraft: {n2['aircraft']}
Ships:    {n2['ships']}
Missiles: {n2['missiles']}
Nukes:    {n2['nukes']}""",
    )
    embed.add_field(
        name="Defender Info:",
        value=f"""
Soldiers: {n1['soldiers']}
Tanks:    {n1['tanks']}
Aircraft: {n1['aircraft']}
Ships:    {n1['ships']}
Missiles: {n1['missiles']}
Nukes:    {n1['nukes']}""",
    )
    embed.set_footer(text="Info Provided By http://politicsandwar.com/api/")
    await self.bot.get_channel(warmonitor).send(embed=embed)


class background:
    def __init__(self, bot):
        self.bot = bot
        self.start_tasks()

    async def trade_cache(self):
        while True:
            try:
                self.bot.logger.info("Caching Trade Prices!")
                tradeprices = []
                links = [
                    "food",
                    "gasoline",
                    "oil",
                    "coal",
                    "uranium",
                    "steel",
                    "iron",
                    "munitions",
                    "lead",
                    "aluminum",
                    "bauxite",
                ]
                for I in links:
                    data = await trade_api(self, I)
                    tradeprices.append(data)
                rsshandle('tradeprices.json', tradeprices)
                self.bot.logger.info("Caching Complete! Trade Prices Updated!")
                await asyncio.sleep(600)
            except Exception as error:
                title = "a background task: 'trade_cache'"
                await raise_error(self.bot, error, title, ctx=None)
                await asyncio.sleep(3600)

    async def mil_stat(self):
        while True:
            try:
                alliances = await alliances_api(self)
                list = []
                for alliance in alliances["alliances"]:
                    aa = await alliance_api(self, alliance["id"])
                    tot = (
                        5 * aa["cities"] * 3000
                        + 5 * aa["cities"] * 250
                        + 5 * aa["cities"] * 18
                        + 3 * aa["cities"] * 5
                    )
                    act = aa["soldiers"] + aa["tanks"] + aa["aircraft"] + aa["ships"]
                    percent = act * 100 / tot
                    list.append(f"{percent:,.2f}")
                    if int(alliance["rank"]) == 50:
                        break
                    rsshandle('warstat.json', list)
                asyncio.sleep(600)
            except Exception as error:
                title = "a background task: 'mil_stat'"
                await raise_error(self.bot, error, title, ctx=None)
                await asyncio.sleep(3600)

    async def war_monitor(self):
        id = 0
        while True:
            try:
                wars = await wars_api(self)
                for I in wars["wars"]:
                    war = await war_api(self, I["warID"])
                    warid = int(I["warID"])
                    break
                if int(warid) != int(id):
                    async with self.bot.pool.acquire() as conn:
                        for guild in self.bot.guilds:
                            for info in war["war"]:
                                warmonitor, pwaa = await conn.fetchrow(
                                    "SELECT warmonitor, pwaa FROM guilds WHERE guild = ($1)",
                                    guild.id,
                                )
                                n1, n2 = await nation_api(
                                    self, info["defender_id"], info["aggressor_id"]
                                )
                                if warmonitor is not 0:
                                    if int(n1["allianceid"]) == pwaa:
                                        await defensive_war(
                                            self, info, n1, n2, warmonitor
                                        )
                                    elif int(n2["allianceid"]) == pwaa:
                                        await aggressive_war(
                                            self, info, n1, n2, warmonitor
                                        )
                        id = warid
                await asyncio.sleep(1)
            except Exception as error:
                title = "a background task: 'war_monitor'"
                await raise_error(self.bot, error, title, ctx=None)
                await asyncio.sleep(3600)

    async def applicant_monitor(self):
        while True:
            try:
                async with self.bot.pool.acquire() as conn:
                    for guild in self.bot.guilds:
                        applicantmonitor, old = await conn.fetchrow(
                            "SELECT applicantmonitor, applicants FROM guilds WHERE guild = ($1)",
                            guild.id,
                        )
                        old = old.split(" ")
                        if applicantmonitor is not 0:
                            data = await applicants(self, guild.id)
                            new = [
                                str(applicant["nationid"])
                                for applicant in data["nations"]
                            ]
                            for id in new:
                                if id not in old:
                                    for applicant in data["nations"]:
                                        if applicant["nationid"] == int(id):
                                            embed = discord.Embed(
                                                title="New Applicant!",
                                                description="A new nation has applied to join your alliance!",
                                                color=randColour(),
                                            )
                                            embed.add_field(
                                                name="Nation",
                                                value=f'[{applicant["nation"]}](https://politicsandwar.com/nation/id={applicant["nationid"]})',
                                            )
                                            embed.add_field(
                                                name="Leader", value=applicant["leader"]
                                            )
                                            embed.add_field(
                                                name="Cities", value=applicant["cities"]
                                            )
                                            embed.add_field(
                                                name="Score", value=applicant["score"]
                                            )
                                            embed.set_footer(
                                                text="Info Provided By http://politicsandwar.com/api/"
                                            )
                                            await self.bot.get_channel(
                                                applicantmonitor
                                            ).send(embed=embed)
                            new = " ".join(new)
                            await conn.execute(
                                "UPDATE guilds SET applicants = ($1) WHERE guild = ($2)",
                                new,
                                guild.id,
                            )
                await asyncio.sleep(60)
            except Exception as error:
                title = "a background task: 'applicant_monitor'"
                await raise_error(self.bot, error, title, ctx=None)
                await asyncio.sleep(3600)

    async def avg_ping(self):
        avg = 0
        count = 0
        pa = 1
        while True:
            try:
                ping_time = int(self.bot.latency * 1000)
                if ping_time < 0:
                    self.bot.logger.warning(
                        "Ping Time dropped into the negatives! Resetting!"
                    )
                    ping_time = int(self.bot.latency * 1000)
                    avg = 0
                    count = 0
                    pa = 1
                avg = ping_time + avg // pa
                self.bot.logger.info(
                    f"Average Latency After {count*60} Minutes: {avg}ms"
                )
                count = count + 1
                pa = pa + 1
                await asyncio.sleep(3600)
            except Exception as error:
                title = "a background task: 'avg_ping'"
                await raise_error(self.bot, error, title, ctx=None)
                await asyncio.sleep(3600)

    async def nsfw_cache(self):
        for guild in self.bot.guilds:
            async with self.bot.pool.acquire() as conn:
                await conn.execute(
                    "UPDATE guilds SET hennext = ($1) WHERE guild = ($2)",
                    0,
                    guild.id,
                )
        while True:
            try:
                async with self.bot.csess.get(f"https://danbooru.donmai.us/posts/random.json") as r:
                    d1 = await r.json(content_type="application/json")
                async with self.bot.csess.get("https://konachan.com/post.json?limit=1000") as r:
                    d2 = await r.json(content_type="application/json")
                list = rsshandle('nsfw_cache.json')
                if d1["rating"] == "q":
                    try:
                        try:
                            if d1["large_file_url"] not in list:
                                list.append(d1["large_file_url"])
                        except:
                            if d1["file_url"] not in list:
                                list.append(d1["file_url"])
                        d2 = random.choice(d2)
                        try:
                            if d2["file_url"] not in list:
                                list.append(d2["file_url"])
                        except:
                            pass
                    except:
                        list = []
                    rsshandle('nsfw_cache.json', list)
                await asyncio.sleep(1)
            except Exception as error:
                title = "a background task: 'nsfw_cache'"
                await raise_error(self.bot, error, title, ctx=None)
                await asyncio.sleep(3600)

    async def autohentai(self):
        while True:
            try:
                unix = int(time.time())
                for guild in self.bot.guilds:
                    async with self.bot.pool.acquire() as conn:
                        henchan, hentime, hennext = await conn.fetchrow(
                            "SELECT henchan, hentime, hennext FROM guilds WHERE guild = ($1)",
                            guild.id,
                        )
                        if henchan is not 0:
                            if unix > hennext:
                                try:
                                    choices = rsshandle('nsfw_cache.json')
                                    await self.bot.get_channel(henchan).send(
                                        embed=discord.Embed(color=randColour()).set_image(
                                            url=random.choice(choices)
                                        )
                                    )
                                    await conn.execute(
                                        "UPDATE guilds SET hennext = ($1) WHERE guild = ($2)",
                                        unix + hentime,
                                        guild.id,
                                    )
                                except Exception as e:
                                    choices = []
                                    rsshandle('nsfw_cache.json', choices)
                    await asyncio.sleep(1)
            except Exception as error:
                title = "a background task: 'autohentai'"
                await raise_error(self.bot, error, title, ctx=None)
                await asyncio.sleep(3600)

    async def activity(self):
        playing=[
            "Politics and War",
            "with the humans!",
            "Requiems Contents: 26% Broken Code, 40% Better than Hydrozen, 30% still can't count",
            "We have updated our privacy policy"
        ]
        watching=[
            "Python 101",
            "The Raatty Chronicles",
            "Dumb Ways to Die",
            "How to not be Requiem",
            "Robo Rat",
            "How to stop watching people sleep",
            "Verin code... he's not very good at it!"
        ]
        activities = [
            discord.Activity(type=0, name=random.choice(playing)),
            discord.Activity(type=3, name=random.choice(watching)),
            discord.Activity(type=2, name=f'{len(self.bot.users)} users')
        ]
        while True:
            try:
                await self.bot.change_presence(activity=random.choice(activities))
                await asyncio.sleep(120)
            except Exception as error:
                title = "a background task: 'activity'"
                await raise_error(self.bot, error, title, ctx=None)
                await asyncio.sleep(3600)

    def start_tasks(self):
        self.bot.loop.create_task(self.activity())
        self.bot.loop.create_task(self.trade_cache())
        self.bot.loop.create_task(self.war_monitor())
        self.bot.loop.create_task(self.applicant_monitor())
        self.bot.loop.create_task(self.avg_ping())
        self.bot.loop.create_task(self.autohentai())
        self.bot.loop.create_task(self.nsfw_cache())
        self.bot.loop.create_task(self.mil_stat())


def setup(bot):
    bot.add_cog(background(bot))
