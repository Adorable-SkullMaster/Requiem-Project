

def randColour():
    """Creates a random colour."""
    from random import randint
    return randint(0, 0xFFFFFF)