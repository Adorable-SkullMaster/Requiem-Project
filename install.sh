#!/usr/bin/env bash

#installs virtual environment to be used by requiem
python -m pip install -U virtualenv
python -m venv venv

cd venv/bin/
source activate

#installs all modules required by requiem into the venv
python3 -m pip install -U git+https://github.com/Rapptz/discord.py@rewrite#egg=discord.py[voice]
python3 -m pip install asyncpg
python3 -m pip install --upgrade aiohttp
python3 -m pip install psutil
python3 -m pip install imgurpython
python3 -m pip install bs4
python3 -m pip install pycparser

#starts the configuration process
python3 setup.py