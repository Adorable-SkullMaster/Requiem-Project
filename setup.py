import json
import asyncpg
import asyncio
from sys import platform
import platform as plt


tokout = """
Please insert your bot token now... If you don't have one yet, you can get one here: 
- https://discordapp.com/developers/applications/me - Don't worry, I'm patient! Take your time...
"""
dbcheck = """
Now for the login credentials of the database! This is an important step, as without the database,
I won't be able to do anything! If you haven't already, please go to https://www.postgresql.org/ 
and download the latest supported version! Please remember the password you assign your database 
during the installation! You will need this if you choose not to create custom login credentials!
"""
dbnameout = """
What is the name of the database? Not sure about this part? No big deal, just type "Default"!
"""
dbuserout = """
What is the username used to login to the database? If you didn't setup a custom user, just type "Default"!
"""
dbpassout = """
What is the password used to login to the database? This part is important! If you didn't setup a custom user,
use the password you created when you installed postgresql!
"""
prefixout = """
Alright! Last step, what would you like my default prefix to be? Not sure? Just type "Default"!
"""
finalout = """
Process is complete, I will now close! To run the bot, just click "requiem_launcher.ps1"!
"""
oidout = """
Next, please provide me with your discord id! I need this for error reporting and other tasks.
This can be obtained by mentioning yourself with a \ prior to your mention. Example: \@someusername"""


def createconfig(path, fileName, data):
    filePathNameWExt = f"./{path}/{fileName}.json"
    with open(filePathNameWExt, "w") as config:
        json.dump(data, config)

async def oscheck():
    if platform == "linux" or platform == "linux2":
        print("I've determined that you are using a Linux distribution! I will proceed under the linux setup process...")
    elif platform == "win32":
        print("I've determined that you are using Windows! I will proceed under the Windows setup process...")

async def setup_start():

    path = "./"
    fileName = "credentials"

    apikey = input(tokout)
    ownerid = input(oidout)

    print(dbcheck)

    await asyncio.sleep(5)

    dbname = input(dbnameout)
    dbuser = input(dbuserout)
    dbpass = input(dbpassout)
    default_prefix = input(prefixout)

    if dbname == "Default":
        dbname = "postgres"
    if dbuser == "Default":
        dbuser = "postgres"
    if prefixout == "Default":
        default_prefix = "r!"

    data = {
        "api key": f"{apikey}",
        "ownerid": int(ownerid),
        "dbname": f"{dbname}",
        "dbuser": f"{dbuser}",
        "dbpass": f"{dbpass}",
        "default_prefix": f"{default_prefix}",
    }

    createconfig(path, fileName, data)


asyncio.get_event_loop().run_until_complete(setup_start())


def credentials():
    with open("credentials.json") as credentials:
        data = json.load(credentials)
    return data



async def setup_finish():
    print(finalout)
    await asyncio.sleep(10)
    exit()


asyncio.get_event_loop().run_until_complete(setup_finish())
